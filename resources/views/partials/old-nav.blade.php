<div class="nav">
    <div class="nav-top container">
        <div class="nav-top__right">
            <div>
                <a href="{{action('PageController@shipping')}}">@lang('main.nav.0')</a>
                <a href="{{action('PageController@policy')}}">@lang('main.nav.1')</a>
            </div>
            <div class="nav-top__right--language">
                <a href="{{ LaravelLocalization::getLocalizedURL('uz') }}"
                   class="uz {{ app()->getLocale() == 'uz' ? 'active' : '' }}">Uz</a>
                <a href="{{ LaravelLocalization::getLocalizedURL('ru') }}"
                   class="ru {{ app()->getLocale() == 'ru' ? 'active' : '' }}">Ru</a>
            </div>
        </div>
    </div>
    <div class="nav-middle container">
        <form action="{{action('PageController@search')}}" method="GET" class="nav-bottom__search nav-middle__search">
            <input type="text" placeholder="@lang('main.nav.20')" name="keyword"/>
            <button class="nav-bottom__search--search nav-middle__search--text">@lang('main.nav.2')</button>
            <button class="nav-bottom__search--magnifier nav-middle__search--magnifier">
                <img src='/img/magnifier.svg' alt="search"/></button>
        </form>
        <div class="nav-middle__logo">
            <a href="{{action('PageController@index')}}">
                <img src="{{asset('img/logo.svg')}}" alt="image">
                <div style="align-items: center;display: flex;flex-direction: column;justify-content: center;">
                    <span class="logo-text">IpakYuli.com</span>
                    <span class="logo-text"
                          style="font-size: 10px;justify-content: center;">Effective B2B Marketplace</span>
                </div>
            </a>
        </div>
        <ul>
            <li><a href="{{action('PageController@about')}}">@lang('main.nav.3')</a></li>
            <li><a href="{{action('PageController@rules')}}">@lang('main.nav.4')</a></li>
        </ul>
        <div class="nav-middle__phone">
            <div class="nav-middle__phone--title">@lang('main.nav.5'):</div>
            <div class="nav-middle__phone--number"><a href="tel:+998712000308">+(99871) 200 03 08</a></div>
        </div>
    </div>
    <div class="nav-bottom">
        <div class="container">
            <div class="section-inner">
                <div class="nav-bottom__category">
                    <div class="nav-bottom__title">
                        <button class="zoom category-button">@lang('main.nav.6')
                            <img src="{{asset('img/arrow.svg')}}" alt="image">
                        </button>
                    </div>
                    <div class="nav-bottom__category-menu menu">
                        <ul class="nav-bottom__category-menu--ul show">
                            @foreach ($categories as $category)
                                <li>
                                    <a href="{{action('PageController@category',$category->alias)}}"><span
                                            class="list-item">{{ $category->name }}</span> <span
                                            class="direction">></span></a>
                                    <div class="menu-second">
                                        <ul>
                                            @foreach ($category->sub_category as $sub_category)
                                                <li>
                                                    <a href="{{ action('PageController@category',$sub_category->alias) }}"><span
                                                            class="list-item">{{$sub_category->name}}</span> <span
                                                            class="direction">></span></a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                            @endforeach
                            <li>
                                <a href="{{action('PageController@categoryAll')}}"><span
                                        class="list-item">@lang('main.nav.7')</span> <span
                                        class="direction">></span></a>
                                <div class="menu-second">
                                    <ul>
                                        @foreach ($all_categories as $category)
                                            <li>
                                                <a href="{{ action('PageController@category',$category->alias) }}"><span
                                                        class="list-item">{{$category->name}}</span> <span
                                                        class="direction">></span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="{{action('PageController@search')}}" method="GET" class="nav-bottom__search">
                    <input type="text" placeholder="@lang('main.nav.20')" name="keyword">
                    <button class="nav-bottom__search--search">@lang('main.nav.2')</button>
                    <button class="nav-bottom__search--magnifier"><img src='/img/magnifier.svg'></button>
                </form>
                <div class="nav-bottom__btns">
                    <div class="nav-bottom__btns--currency">
                        <button>
                            <div class="nav-bottom__btns--img"><img src="/img/new_currency.svg" alt="image"></div>
                            <span class="account-text ignore-item">
                                <span class="top">@lang('main.nav.8')</span>
                                <span class="text-center">UZS</span>
                            </span>
                        </button>
                    </div>
                    <div class="nav-bottom__btns-account ignore-item">
                        <button class="login-btn ignore-item">
                            <span class="nav-bottom__btns--img ignore-item">
                                <img src="/img/Layer 2.svg" alt="image" class="ignore-item">
                            </span>
                            <span class="account-text ignore-item">
                                <span class="top ignore-item">@lang('main.nav.9')</span>
                                <span class="ignore-item">@lang('main.nav.10')</span>
                            </span>
                        </button>
                        @if(!Auth::check())
                            <div class="nav-bottom__btns--login login-hidden show-item ignore-item">
                                <div class="nav-bottom__btns--login_wrapper ignore-item">
                                    <button class="ignore-item">
                                        <div class="nav-bottom__btns--img">
                                            <img src="/img/Layer 2.svg" alt="image" class="ignore-item">
                                        </div>
                                        <span class="account-text ignore-item">
                                        <span class="top">@lang('main.nav.9')</span>
                                        <span>@lang('main.nav.10')</span>
                                    </span>
                                    </button>
                                </div>
                                <form action="{{ action('AuthController@login') }}" method="POST"
                                      class="nav-bottom__btns--login_form ignore-item">
                                    @csrf
                                    <input type="email" name="email" placeholder="Электронная почта"/>
                                    <input type="password" name="password" placeholder="Пароль"/>
                                    <div class="nav-bottom__btns-account-login__links ignore-item">
                                        {{--                                        <a href="#" class="no-opacity">Забыли пароль ?</a>--}}
                                        <a href="{{ action('PageController@auth') }}">@lang('main.nav.11')</a>
                                    </div>
                                    <div class="nav-bottom__btns--login_submit ignore-item">
                                        <button class="ignore-item">@lang('main.nav.12')</button>
                                    </div>
                                </form>
                            </div>
                        @else
                            <div class="nav-bottom__btns-account-logout white-box login-hidden show-item ignore-item">
                                <div class="nav-bottom__btns--login_wrapper ignore-item">
                                    <button class="ignore-item">
                                        <span class="nav-bottom__btns--img ignore-item">
                                            <img src="/img/Layer 2.svg" alt="image" class="ignore-item"></span>
                                        <span class="account-text ignore-item">
                                        <span class="top ignore-item">@lang('main.nav.9')</span>
                                        <span class="ignore-item">@lang('main.nav.10')</span>
                                    </span>
                                    </button>
                                </div>
                                <div class="nav-bottom__btns-account-logout__wrapper ignore-item">
                                    <div
                                        class="nav-bottom__btns-account-logout__name ignore-item">{{Auth::user()->name}}</div>
                                    <div class="nav-bottom__btns-account-logout__order ignore-item">
                                        <a href="{{action('PageController@process')}}"
                                           class="ignore-item">@lang('main.nav.13')</a>
                                        <a href="{{action('PageController@personal')}}"
                                           class="ignore-item">@lang('main.nav.14')</a>
                                        <a href="{{action('PageController@messages')}}"
                                           class="ignore-item">@lang('main.nav.15')</a>
                                    </div>
                                    <div class="nav-bottom__btns-account-logout__exit ignore-item">
                                        <a href="{{action('AuthController@logout')}}" class="ignore-item">@lang('main.nav.16')</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="nav-bottom__btns-cart">
                        <a href="{{action('PageController@basket')}}">
                            <div class="nav-bottom__btns--img">
                                <span class="wrapper-img">
                                    <img src="/img/Layer 3 (1).svg" alt="image">
                                    <span class="wrapper-img__count">@{{ basketCount }}</span>
                                </span>
                            </div>
                            <span class="account-text last">
                                <span class="top">@lang('main.nav.17')</span>
                                <span class="bottom">@lang('main.nav.18')</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="nav-bottom navbar-fixed">
    <div class="container navbar-fixed-wrapper">
        <div class="section-inner navbar-fixed-wrapper">
            <div class="nav-bottom__category navbar-fixed__category">
                <div class="nav-bottom__logo navbar-fixed__logo" id="logo">
                    <a href="/">
                        <img src="{{asset('img/logo.svg')}}" alt="logo">
                    </a>
                </div>
                <div class="nav-bottom__title navbar-fixed__title">
                    <button class="navbar-fixed__button zoom category-button">@lang('main.nav.6')<img
                            src="{{ asset('img/arrow.svg') }}" alt="image"></button>
                </div>
                <div class="nav-bottom__category-menu menu">
                    <ul class="nav-bottom__category-menu--ul">
                        @foreach ($categories as $category)
                            <li>
                                <a href="{{action('PageController@category',$category->alias)}}"><span
                                        class="list-item">{{ $category->name }}</span> <span class="direction">></span></a>
                                <div class="menu-second">
                                    <ul>
                                        @foreach ($category->sub_category as $sub_category)
                                            <li>
                                                <a href="{{ action('PageController@category',$sub_category->alias) }}"><span
                                                        class="list-item">{{$sub_category->name}}</span> <span
                                                        class="direction">></span></a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @endforeach
                        <li>
                            <a href="{{action('PageController@categoryAll')}}"><span
                                    class="list-item">@lang('main.nav.7')</span> <span class="direction">></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <form action="{{action('PageController@search')}}" method="GET"
                  class="nav-bottom__search navbar-fixed__search">
                <input type="text" placeholder="@lang('main.nav.20')" name="keyword">
                <button class="nav-bottom__search--search">@lang('main.nav.2')</button>
                <button class="nav-bottom__search--magnifier">
                    <img src='/img/magnifier.svg'/>
                </button>
            </form>
            <div class="nav-bottom__btns">
                <div class="nav-bottom__btns--currency">
                    <button>
                        <div class="nav-bottom__btns--img ignore-item"><img src="/img/new_currency.svg" alt="image">
                        </div>
                        <span class="account-text ignore-item">
                            <span class="top">@lang('main.nav.8')</span>
                            <span class="text-center">UZS</span>
                        </span>
                    </button>
                </div>
                <div class="nav-bottom__btns-account ignore-item">
                    <button class="login-btn ignore-item">
                        <span class="nav-bottom__btns--img ignore-item"><img src="/img/Layer 2.svg" alt="image"
                                                                             class="ignore-item"></span>
                        <span class="account-text ignore-item">
                            <span class="top ignore-item">@lang('main.nav.9')</span>
                            <span class="ignore-item">@lang('main.nav.10')</span>
                        </span>
                    </button>
                    @if(!Auth::check())
                        <div class="nav-bottom__btns--login login-hidden show-item">
                            <div class="nav-bottom__btns--login_wrapper ignore-item">
                                <button class="ignore-item">
                                    <span class="nav-bottom__btns--img ignore-item"><img src="/img/Layer 2.svg"
                                                                                         alt="image"
                                                                                         class="ignore-item"></span>
                                    <span class="account-text ignore-item">
                                        <span class="top ignore-item">@lang('main.nav.9')</span>
                                        <span class="ignore-item">@lang('main.nav.10')</span>
                                    </span>
                                </button>
                            </div>
                            <form action="{{ action('AuthController@login') }}" method="POST"
                                  class="nav-bottom__btns--login_form ignore-item">
                                @csrf
                                <input type="email" name="email" placeholder="Электронная почта" class="ignore-item"/>
                                <input type="password" name="password" placeholder="Пароль" class="ignore-item"/>
                                <div class="nav-bottom__btns-account-login__links ignore-item">
                                    {{--                                    <a href="#" class="no-opacity ignore-item">Забыли пароль ?</a>--}}
                                    <a href="{{ action('PageController@auth') }}"
                                       class="ignore-item">@lang('main.nav.11')</a>
                                </div>
                                <div class="nav-bottom__btns--login_submit ignore-item">
                                    <button class="ignore-item">@lang('main.nav.12')</button>
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="nav-bottom__btns-account-logout white-box ignore-item login-hidden show-item">
                            <div class="nav-bottom__btns--login_wrapper ignore-item">
                                <button class="ignore-item">
                                    <span class="nav-bottom__btns--img ignore-item"><img src="/img/Layer 2.svg"
                                                                                         alt="image"
                                                                                         class="ignore-item"></span>
                                    <span class="account-text ignore-item">
                                        <span class="top ignore-item">@lang('main.nav.9')</span>
                                        <span class="ignore-item">@lang('main.nav.10')</span>
                                    </span>
                                </button>
                            </div>
                            <div class="nav-bottom__btns-account-logout__wrapper ignore-item">
                                <div
                                    class="nav-bottom__btns-account-logout__name ignore-item">{{Auth::user()->name}}</div>
                                <div class="nav-bottom__btns-account-logout__order ignore-item">
                                    <a href="{{action('PageController@process')}}"
                                       class="ignore-item">@lang('main.nav.13')</a>
                                    <a href="{{action('PageController@personal')}}"
                                       class="ignore-item">@lang('main.nav.14')</a>
                                    <a href="{{action('PageController@messages')}}"
                                       class="ignore-item">@lang('main.nav.15')</a>
                                </div>
                                <div class="nav-bottom__btns-account-logout__exit ignore-item">
                                    <a href="{{action('AuthController@logout')}}" class="ignore-item">@lang('main.nav.16')</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="nav-bottom__btns-cart">
                    <a href="{{action('PageController@basket')}}">
                        <div class="nav-bottom__btns--img">
                            <span class="wrapper-img">
                                <img src="/img/Layer 3 (1).svg" alt="image">
                                <span class="wrapper-img__count">@{{ basketCount }}</span>
                            </span>
                        </div>
                        <span class="account-text last">
                            <span class="top">@lang('main.nav.17')</span>
                            <span class="bottom">@lang('main.nav.18')</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blur"></div>
