<a class="product__big" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
    <img src="{{ asset($product->image[0]) }}" class="product__big__image" alt="product__big">
    <span class="absolute-hot">
        <img src="{{asset('img/hot.svg')}}" alt="hot">
    </span>
    <div class="product__big-bottom">
        <span class="product__big-name">{{$product->name}}</span>
        <div
            class="product__big-price">{{ count($product->range) > 1 ? number_format($product->range[0]).' - '.number_format($product->range[1])  . ' UZS': ($product->range[0] != 0 ? number_format($product->range[0]) . ' UZS': 'Договорная') }} </div>
        <div class="product__big-count">
            <span class="gray">@lang('main.index.1'):</span>
            <b class="bold">{{$product->min_count}}</b>
            <span class="gray">{{$product->count_type ?? 'шт'}}</span>
        </div>
    </div>
</a>
