<footer class="footer">
    <div class="container d-flex flex-wrap align-items-center justify-content-between">
        <div class="col-lg-1 lg-none">
            <a href="{{action('PageController@index')}}">
                <img src="{{asset('img/logo.svg')}}" alt="logo">
            </a>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="footer-title">Подробности:</div>
            <ul class="d-flex flex-wrap">
                <li><a href="{{action('PageController@about')}}">@lang('main.nav.3')</a></li>
                <li><a href="{{action('PageController@rules')}}">@lang('main.nav.4')</a></li>
            </ul>
            <ul class="d-flex flex-wrap">
                <li><a href="{{action('PageController@shipping')}}">@lang('main.nav.0')</a></li>
                <li><a href="{{action('PageController@policy')}}">@lang('main.nav.1')</a></li>
            </ul>
        </div>
        <div class="col-lg-3 sm-none col-md-3">
            <div class="footer-title">@lang('main.nav.5'):</div>
            <div><a href="tel:+998712000308" class="footer-phone">+(99871) 200 03 08</a></div>
            <div><a class="footer-email" href="mailto:index@ipakyuli.com">index@ipakyuli.com</a></div>
        </div>
        <div class="footer-sm-en col-lg-3 col-md-4 col-sm-12 d-flex flex-md-column align-items-sm-center justify-content-sm-between">
            <div class="socials d-flex align-items-center justify-content-between">
                <a href=""><img src="{{asset('img/telegram.svg')}}" alt="telegram"></a>
                <a href=""><img src="{{asset('img/facebook.svg')}}" alt="facebook"></a>
                <a href=""><img src="{{asset('img/instagram.svg')}}" alt="instagram"></a>
            </div>
            <a href="https://indev.uz/" target="_blank" class="developed d-flex align-items-center justify-content-end">
                Developed by <b class="bold"> InDev</b> <img class="indev" src="{{asset('img/indev.svg')}}" alt="indev"/>
            </a>
        </div>
    </div>
</footer>
