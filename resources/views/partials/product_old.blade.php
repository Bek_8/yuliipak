<a class="main-card__item" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
{{--    <div class="main-card__item--images big-cards__item-bottom--prodaj">--}}
{{--        <img src="{{ asset('img/Star 3.svg') }}" alt="star" />--}}
{{--        <img src="{{ asset('img/Star 3.svg') }}" alt="star">--}}
{{--        <img src="{{ asset('img/Star 3.svg') }}" alt="star">--}}
{{--        <img src="{{ asset('img/Star 3.svg') }}" alt="star">--}}
{{--        <img src="{{ asset('img/Star 5.svg') }}" alt="star">--}}
{{--    </div>--}}
    <div class="main-card__item--stock">
{{--        <div class="main-card__item--stock_percent">-25%</div>--}}
    </div>
    <div class="main-card__item--img"><img src="{{ asset($product->image[0]) }}" alt="photo" /></div>
    <div class="main-card__item--item-info">
        <div class="main-card__item--item-info__top">
            <span class="main-card__item--item-info__top--title">{{$product->name}}</span>
            <span class="main-card__item--item-info__top--subtitle" style="opacity: 0;margin-bottom: 0;margin-top: 0;">{{$product->vendor->name}}</span>
        </div>
        <div class="main-card__item--item-info__bottom">
            <div class="main-card__item--item-info__bottom--cost">{{ count($product->range) > 1 ? number_format($product->range[0]).' - '.number_format($product->range[1]) : number_format($product->range[0])  }} UZS</div>
        </div>
    </div>
</a>
