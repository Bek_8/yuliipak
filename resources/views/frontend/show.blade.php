@extends('layouts.frontend')

@section('content')
    {{--    <div class="breadcrumb">--}}
    {{--        @foreach($product->category->parent_category as $crumb)--}}
    {{--            <a href="{{$crumb->alias}}">{{$crumb}}</a>--}}
    {{--        @endforeach--}}
    {{--        <a href="{{$product->category->alias}}">{{$product->category}}</a>--}}
    {{--    </div>--}}
    <show-product :add-item="addItem"></show-product>
    <br><br>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('css/swiper.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection
@section('script')
{{--    <script type="text/javascript" src="{{asset('js/zoom.js')}}"></script>--}}
    <script src="{{asset('js/swiper.js')}}"></script>
    <script src="{{asset('js/fancybox.js')}}"></script>
    <script>
        let vm;
        Vue.component('show-product', {
            props: ['addItem'],
            data() {

                return {
                    count: 1,
                    size: @if($product->type != 'default')  {!! $product->size !!} @else [] @endif,
                    countPriceDay: {!! $product->type == 'default' ? $product->price : $product->size->first()->type->first()->price !!},
                    productType: "{{ $product->type }}",
                    productId: {{$product->id}} ,
                    countType: "({{$product->count_type}})",
                    {{--              minPrice: {{$product->min_price}} ,--}}
                    minCount: {{$product->min_count}} ,
                    vendorId: {{$product->vendor->id}} ,
                    currentSizeId: {{$product->type != 'default' ? $product->size->first()->id : 0}} ,
                    currentTypeId: {{$product->type != 'default' ? $product->size->first()->type->first()->id : 0}},
                    currency: {{ $product->currency == 'usd' ? $product->dollar * 1 : 1  }},
                    type: @if($product->type != 'default')  {!! $product->size[0]->type !!} @else [] @endif,
                    countries: {!! $countries !!},
                    cities: {!! $cities !!},
                    country: 0,
                    city: 0,
                    showProduct : false
                }
            },
            mounted() {
                vm = this;
                // this.type = this.productType != 'default' ? this.size[0].type : [];
            },
            computed: {
                onPriceCountDay: function () {
                    let getPrice = this.countPriceDay.filter(e => (parseInt(e[0]) <= this.count));
                    getPrice = getPrice[getPrice.length - 1];
                    if (getPrice === undefined) getPrice = this.countPriceDay[0];
                    return parseInt(getPrice[1]) * this.count;
                },
                checkoutPrice() {
                    if (this.count >= this.minCount) {
                        return 1;
                    }
                    return 0;
                },
                getRelatedCities() {
                    return this.cities.filter(item => item.country_id == this.country);
                }
            },
            methods: {
                setSize: function ({type, target}) {
                    let getValue = parseInt(target.value);
                    this.type = this.size.find(e => e.id === getValue).type;
                    this.currentTypeId = this.type[0].id;
                    this.countPriceDay = JSON.parse(this.type[0].price);
                },
                setCountry: function (value) {
                    this.count = value;
                },
                setType: function ({target}) {
                    let getValue = parseInt(target.value);
                    let getType = this.type.find(e => e.id === getValue);
                    this.countPriceDay = JSON.parse(getType.price);
                },
                increment: function () {
                    if (this.count < 100000) this.count = this.count + 1;
                },
                setCount: function (count) {
                    let number = parseInt(count);
                    if (number > 0 && number < 300) {
                        this.count = number;
                    }
                },
                decrement: function () {
                    if (this.count > 1) this.count = this.count - 1;
                },
                addToBasket: function () {
                    if (this.checkoutPrice === 1) {
                        this.addItem(this.count, this.productId, this.currentSizeId, this.currentTypeId, this.vendorId);
                    } else {
                        $.notify("@lang('main.show.0') " + this.minCount, "warn");
                    }
                },
                resetCity: function () {
                    this.city = 0;
                    document.querySelector('#city_id').selectedIndex = 0;
                }
            },
            template: `
    <div class="container mt-20">
        <div class="hidden-item">
            <div class="hidden-item-wrapper">
                <div class="hidden-item-wrapper-name">
                    <div>
                        <span class="hidden-item-wrapper-name__main">{{$product->name}} </span>
                        <span class="hidden-item-wrapper-name__second second-style">{{$product->vendor->name}}</span>
                    </div>
                </div>
                <div class="description-item-text__top--guarantee">
                    <div class="description-item-text__top--guarantee_top">
                        <div class="description-item-text__top--guarantee_text">@lang('main.show.2'):</div>
                        <img src="{{ asset('backend/images/flags/'.$product->vendor->country->flag.'.svg') }}" style="width: 2.2rem; height: 1.2rem; border-radius: 10px;" alt="">
                    </div>
                    <div class="description-item-text__top--guarantee_bottom">
                        <div class="description-item-text__top--guarantee_text">@lang('main.show.1'):</div>
                        <div class="description-item-text__top--guarantee_tag description-item-text__top--guarantee_label">да</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="description-item">
            <div class="description-item-wrapper">
                <div class="description-item-img">
                <div class="product-slider">
                  <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($images as $image)
                        <div class="swiper-slide">
                          <a data-fancybox="product" href="{{asset($image)}}">
                              <img src="{{asset($image)}}" alt="product" />
                          </a>
                        </div>
                        @endforeach
            </div>
            <div class="swiper-button-prev"><img src="{{asset('img/arrow-prev.svg')}}" alt="arrow" /></div>
            <div class="swiper-button-next"><img src="{{asset('img/arrow-next.svg')}}" alt="arrow" /></div>
          </div>
    </div>
</div>
<div class="description-item-text">
<div class="description-item-text__top">
    <div class="description-item-text__top--name">
        <div>
            <h3 class="description-item-text__top--name_main">{{ $product->name }} </h3>
                                <p class="description-item-text__top--name_second second-style">{{ $product->vendor->name }}</p>
                            </div>
                        </div>
                        <div class="description-item-text__top--guarantee">
                            <div class="description-item-text__top--guarantee_top">
                                <div class="description-item-text__top--guarantee_text">@lang('main.show.7'):</div>
                                <div class="description-item-text__top--guarantee_tag">
                                    <img src="{{ asset('backend/images/flags/'.$product->vendor->country->flag.'.svg') }}" style="width: 2.2rem; height: 1.2rem; border-radius: 10px;" alt="">
                                </div>
                            </div>
                            <div class="description-item-text__top--guarantee_bottom">
                                <div class="description-item-text__top--guarantee_text">@lang('main.show.4') :</div>
                                <div class="description-item-text__top--guarantee_tag description-item-text__top--guarantee_label">@if(strlen($product->vendor->nds) > 2)  @lang('main.show.5') @else  @lang('main.show.6') @endif</div>
                            </div>
                        </div>
                    </div>
                    <form class="description-item-text__bottom" onsubmit="event.preventDefault()">
                        <div class="description-item-text__bottom--left">
                            <table class="description-item-text__bottom--left-table">
                                <thead>
                                    <tr>
                                        <th>@lang('main.show.8') @{{ countType }}</th>
                                        <th>@lang('main.show.9') @{{ countType }}</th>
                                        <th>@lang('main.show.10')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="elem in countPriceDay">
                                        <td>@{{elem[0]}}</td>
                                        <td>@{{elem[1] ?  elem[1] * parseInt(currency) != 0 ? elem[1] * parseInt(currency) + ' UZS' : '@lang('main.show.11')' : ''}}</td>
                                        <td>@{{elem[2]}} @lang('main.show.12')</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="description-item-text__bottom--right">
                            <div class="description-item-text__bottom--right_wrapper white-box">
                                <div class="description-item-text__bottom--right_top">
                                    <div class="description-item-text__bottom--right_top-top">
                                        <div class="description-item-text__bottom--right_quantity">
                                            <div class="description-title">@lang('main.show.13'):</div>
                                            <div class="description-item-text__bottom--right_quantity-navs">
                                                <button v-on:click="decrement"><img src="{{ asset('img/minus-button.svg') }}" alt=""></button>
                                                    <input type="number" min="1" v-on:input="setCount($event.target.value)" :value="count" class="number" />
                                                <button v-on:click="increment"><img src="{{ asset('img/plus-button.svg') }}" style="max-width: 80px;text-align: center;" alt=""></button>
                                            </div>
                                        </div>
                                       @if($product->type == 'structured')
            <div class="description-item-text__bottom--right_wl">
                <div class="description-item-text__bottom--right_color">
                    <div class="description-title">@lang('main.show.14'):</div>
                                                <div class="description-item-text__bottom--right_color-navs">
                                                    <div class="description-item-text__bottom--right_color-navs__radio" v-for="(currentSize , key) in size">
                                                        <input type="radio" :id="'color'+ key" name="size" :value="currentSize.id" v-model="currentSizeId" @input="setSize">
                                                        <label :for="'color'+ key">
                                                            <img :src="'/'+currentSize.image" />
                                                            <span><img src="{{asset('img/check.svg')}}" alt=""></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="description-item-text__bottom--right_size">
                                                <div class="description-title">@lang('main.show.15'):</div>
                                                <div class="description-item-text__bottom--right_size-navs">
                                                        <div class="description-item-text__bottom--right_size-navs__radio" v-for="(currentType , key) in type" v-if="currentType.product_size_id == currentSizeId">
                                                            <input type="radio" :id="'radio'+key" name="type" :value="currentType.id" v-model="currentTypeId" @input="setType" >
                                                            <label :for="'radio'+key">@{{currentType.name}}</label>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                       @endif
            </div>
            <div class="description-item-text__bottom--right_top-bottom">
                <div class="description-item-text__bottom--right_top-cost">
                    <div class="description-item-text__bottom--right_top-cost__title">@lang('main.show.16'):</div>
                    <div class="description-item-text__bottom--right_top-cost__money">@{{ onPriceCountDay == 0 ? '@lang('main.show.11')' : onPriceCountDay * parseInt(currency) + ' UZS' }}</div>
                </div>
                <div class="description-item-text__bottom--right_top-total">
                    <div class="description-item-text__bottom--right_top-total__title"><span class="title2">@lang('main.show.18'):</span>
                        <div class="description-item-text__bottom--right_top-total__title--tag"><img src="{{asset('img/total.svg')}}" alt="total"></div>
                                            </div>
                                            <div class="description-item-text__bottom--right_top-total__money">
                                                @{{ onPriceCountDay == 0 ? '@lang('main.show.11')' : onPriceCountDay * parseInt(currency) + ' UZS' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="description-item-text__bottom--right_button">
                                <button v-on:click="addToBasket" v-if="onPriceCountDay > 0">@lang('main.show.19')</button>
                                <form style="width:100%;" action={{action('MessageController@store',$product->id)}} method="POST" v-if="onPriceCountDay == 0 ">
                                   @csrf
            <input type="hidden" name="count" :value="count" />
            <input type="hidden" name="size_id" :value="currentSizeId" />
            <input type="hidden" name="type_id" :value="currentTypeId" />
            <div>
            <label class="current__adress--top_title" style="margin-top:0;">@lang('main.show.20')</label>
                                    <select name="country_id" id="country_id" v-model="country" @change="resetCity" class="form-control custom-select" required="required">
                                        <option disabled>@lang('main.show.21') </option>
                                        <option v-for="(city , key) in countries" v-bind:value="city.id">@{{ city.name }}</option>
                                    </select>
                                    <select name="city_id" id="city_id" class="form-control custom-select" v-model="city" required="required">
                                        <option disabled="disabled">@lang('main.show.21') </option>
                                        <option v-for="(city , key) in getRelatedCities" v-bind:value="city.id">@{{ city.name }}</option>
                                    </select>
                                    </div>
                                    <br/>
                                    <button>@lang('main.show.22')</button>
                                </form>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="product-informations">
            <div class="product-informations-wrapper white-box d-flex">
                <div class="product-title"> <span v-bind:class="!showProduct ? 'active' : ''" @click="showProduct = false;">@lang('main.show.23')</span> <span v-bind:class="showProduct ? 'active' : ''" @click="showProduct = true;">Информация о компании </span> </div>
            </div>
        </div>
        <div class="product-informations-middle white-box" v-if="showProduct == true">
            <div class="product-informations-middle__info" style="flex-wrap:wrap;">
                {!! $product->description !!}
            </div>
        </div>
        <div class="product-informations-middle white-box" v-else>
            <div class="document-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                    @foreach($product->vendor->image as $image)
                        <div class="swiper-slide">
                            <img src="{{ asset($image) }}" height="150" alt="vendor">
                        </div>
                    @endforeach
                    </div>
                    <div class="swiper-button-prev"><img src="{{asset('img/arrow-prev.svg')}}" alt="arrow" /></div>
                    <div class="swiper-button-next"><img src="{{asset('img/arrow-next.svg')}}" alt="arrow" /></div>
                </div>
            </div>
            <div class="product-informations-middle__info">
                <div class="information flex-direction-column">
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Тип Бизнеса </div>
                        <div class="info"> @foreach($product->vendor->categories as $category) {{$category->name}} @endforeach </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Основные продукты </div>
                        <div class="info"> @foreach($product->vendor->categories as $category) {{$category->name}} @endforeach </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Гувохнома </div>
                        <div class="info"> - </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Сертификаты </div>
                        <div class="info"> - </div>
                    </div>
                </div>
                <div class="information flex-direction-column">
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Патенты </div>
                        <div class="info"> - </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Страна - Регион </div>
                        <div class="info"> {{$product->vendor->country->name}} - {{$product->vendor->city->name}}</div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Кол-во сотрудников </div>
                        <div class="info"> {{$product->vendor->workers}} </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Год основания </div>
                        <div class="info"> {{$product->vendor->open_date}} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="section-product">
        <div class="title-divider d-flex align-items-center justify-content-between">
            <div class="main-title">@lang('main.show.27')</div>
            <a href="{{action('PageController@showVendor',$product->vendor->alias)}}" class="main-all">
                Больше <img src="{{asset('img/arrow-right.svg')}}" alt="arrow"/>
            </a>
        </div>
        <div class="d-flex flex-wrap products__wrap">
            @foreach($product->vendor->product as $key => $product)
                @include('partials.product')
            @endforeach
        </div>
    </div>
    <br/>
    <br/>
`
        })
        $(document).ready(function () {
            const swiper = new Swiper('.swiper-container', {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });

        });

    </script>
@endsection
