@extends('layouts.frontend')

@section('content')
    <div class="collections-wrap auth-wrap d-flex align-items-center justify-content-center">
        <div class="container">
            <form action="{{action('AuthController@verification')}}" method="POST"  class="auth">
                @csrf
                <img src="{{asset('img/logo.svg')}}" alt="logo">
                <div class="phone d-flex align-items-center justify-content-between">
                    <div class="left">
                        <img src="{{asset('img/profile.svg')}}" alt="profile"/>
                    </div>
                    <input type="tel" name="phone" placeholder="Номер телефона" required="required"/>
                </div>
                <a href="{{action('PageController@policy')}}" target="_blank" class="rights">
                    Войдя в аккаунт вы соглашаетесь с
                    <span class="main-color">условиями использования приложения</span>
                </a>
                <button class="btn" type="submit">Войти в аккаунт</button>
            </form>
        </div>
    </div>
@endsection
