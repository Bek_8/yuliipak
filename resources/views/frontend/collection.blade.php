@extends('layouts.frontend')

@section('content')
    <br>
    <div class="section-product">
        <div class="container">
            <div class="category__all">Сборники</div>
            <div class="section-product-wrapper collections__all">
                @foreach($collections as $collection)
                    <div class="collections d-flex align-items-center justify-content-between">
                        <div class="collections-inner">
                            <div class="title">{{$collection->name}}</div>
                            <div class="description">{{$collection->description}}</div>
                            <a href="{{action('PageController@showCollection',$collection->id)}}" class="btn">Посмотреть
                                больше</a>
                        </div>
                        <div class="collections-right d-flex align-items-center justify-content-end">
                            @foreach($collection->products as $key => $product)
                                @if($key < 4)
                                    @include('partials.product')
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <br>
@endsection
