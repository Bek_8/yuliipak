@extends('layouts.frontend')

@section('content')
    <div class="container">
        <p><strong>Договор</strong></p>
        <p><strong>о предоставлении услуг </strong></p>
        <p><strong>Интернет-магазина</strong></p>
        <p><span>ООО "</span><span>PLATFORMA</span><span
            >IPAKYULI</span><span>"</span> именуемый далее по тексту &laquo;Исполнитель&raquo;
            с одной стороны, и физическое или юридическое лицо, оформившееся на сайте <u>ipakyuli.com</u>, именуемое
            далее по тексту &laquo;Покупатель&raquo; с другой стороны, и компания, предприниматель, осуществляющий
            продажу товаров или услуг дистанционно через сайт<u>ipakyuli.com,</u>именуемый далее по тексту &laquo;Продавец&raquo;&nbsp;&mdash;
            каждый по отдельности именуемый &laquo;Сторона&raquo;, а вместе &ndash; &laquo;Стороны&raquo;, заключили
            настоящий Договор - оферты (далее - Договор) о нижеследующем:</p>
        <p><strong>1. Термины в Договоре</strong></p>
        <p>1.1. В&nbsp;настоящем Договоре, если контекст не&nbsp;требует иного, нижеприведенные термины имеют следующие
            значения:</p>
        <ul>
            <li>
                <p><strong>&laquo;</strong><u><strong>ipakyuli.com</strong></u><strong>&raquo;</strong>&mdash; открытый
                    сайт для свободного визуального ознакомления, публично доступный, принадлежащий Продавцу ресурс,
                    содержащий информацию об&nbsp;ассортименте товара, цене, условиях доставки товара Покупателю.</p>
            </li>
            <li>
                <p><strong>&laquo;Сайт&raquo;</strong>&nbsp;- открытый для общего пользования Интернет-ресурс,
                    размещенный Исполнителем в сети по адресу: <u>www.</u><u>ipakyuli.com</u></p>
            </li>
            <li>
                <p><strong>&laquo;Исполнитель&raquo; </strong>&mdash;<span>ООО "</span><span
                    >PLATFORMA</span><span>IPAKYULI</span><span>"</span>
                </p>
            </li>
            <li>
                <p><strong>&laquo;Продавец&raquo;</strong>&nbsp;&mdash; компания, предприниматель, осуществляющий
                    продажу товаров дистанционно - через сайт;</p>
            </li>
            <li>
                <p><strong>&laquo;Покупатель&raquo;&nbsp;</strong>&mdash; физическое или юридическое лицо, заключившее с&nbsp;Продавцом
                    настоящий Договор-оферту на&nbsp;условиях, содержащихся в&nbsp;Договоре-оферте;</p>
                <ul>
                    <ul>
                        <ul>
                            <li>
                                <p><strong>&laquo;Оферта&raquo;</strong> &mdash; настоящий Договор, включая все его
                                    приложения;</p>
                            </li>
                        </ul>
                    </ul>
                </ul>
            </li>
            <li>
                <p><strong>&laquo;Акцепт&raquo; </strong>&mdash;полное и&nbsp;безоговорочное принятие Покупателем или
                    Продавцом условий настоящего Договора-оферты;&nbsp;</p>
            </li>
            <li>
                <p><strong>&laquo;Товар&raquo;&nbsp;</strong>&mdash; товар, продукция, информация о&nbsp;наименовании,
                    ассортименте и&nbsp;цене которых размещается на&nbsp;сайте <span>i</span>pakyuli.com;&nbsp;
                </p>
            </li>
            <li>
                <p><strong>&laquo;Интернет-заказ&raquo;&nbsp;</strong>&mdash; отдельные позиции из&nbsp;ассортиментного
                    перечня Товара, указанные Покупателем при оформлении заявки на&nbsp;сайте <span
                    >i</span>pakyuli.com.</p>
            </li>
            <li>
                <p><strong>&laquo;Услуги&raquo;</strong>&mdash; купле/продажа товаров между Продавцами и
                    Покупателем<span>;</span></p>
            </li>
            <li>
                <p><strong>&laquo;Учетная запись&raquo;</strong>&nbsp;&ndash; это специальная страница, создаваемая
                    Покупателем или Продавцом на платформе сайта, предназначенная для отображения опубликованной
                    информации Покупателем или Продавцом. Учетная запись является открытой страницей и опубликованная на
                    нем информация является доступной для всех его пользователей, без каких либо ограничений.</p>
            </li>
            <li>
                <p><strong>&laquo;Личный кабинет&raquo;</strong>&nbsp;&mdash; это специальный раздел сайта, доступный
                    только соответствующему Покупателю или Продавцу. Через личный кабинет осуществляется общее
                    управление публикуемой на учетной записи информации Покупателя или Продавца.</p>
            </li>
        </ul>
        <p><strong>2. Предмет Договора</strong></p>
        <p><span>2</span>.1. Настоящий договор является публичным договором-офертой (предложением)
            Исполнителя.</p>
        <p>2.2. Исполнитель предоставляет посреднические услуги по купле/продажи товаров между Продавцами и Покупателем,
            в порядке и на условиях данного Договора.</p>
        <p><span>2</span>.3.Покупатель принимает условия настоящего Договора при оформлении заказа на
            сайте <span>i</span>pakyuli.com.</p>
        <p>2.4. <span>Продавец принимает условия </span>настоящего Договора при оформлении услуг на
            сайте <span>i</span>pakyuli.com.</p>
        <p><span>2.5. </span>Исполнитель оставляет за собой право в одностороннем порядке вносить
            изменения в настоящий Договор, в связи с чем Покупатель обязуется регулярно отслеживать изменения в
            Договоре, размещенном по адресу<span>i</span>pakyuli.com.</p>
        <p>2.6. Покупатель, оформляя заказ, может предварительно рассчитать стоимость заказанной вещи, обратиться к
            работнику <span>Продавца</span>, контакты расположены на сайте <span>i</span>pakyuli.com<span
            >.</span></p>
        <p>2.7. Перед покупкой Товара на сайте <span>i</span>pakyuli.com Покупатель обязан ознакомиться с
            условиями настоящего Договора</p>
        <p><strong>3. Оформление заказа и работа с заказом</strong></p>
        <p>3.1. Для получения услуг по доставке товара Покупатель самостоятельно оформляет заказ, предварительно оценив
            качество, параметры и характеристики товара. За достоверность данных, указанных Покупателем при регистрации
            и заказе товара ответственность несёт сам Покупатель.</p>
        <p>3.2 Покупатель осуществляет выбор товара на сайте <span>i</span>pakyuli.comи помещает его в
            раздел для оформления заказа. Помещение товара в форму не является заказом, Покупатель до начала оформления
            заказа может самостоятельно удалять и добавлять товары в форме заказа.</p>
        <p>3.3 Покупатель помещает ссылки на Товары, которые желает приобрести и проходит процедуру оформления
            Заказа.</p>
        <p>3.4. Исполнитель начинает работу с Заказом только после получения оплаты стоимости Товаров, которая указана
            на сайте <span>i</span>pakyuli.com, включенных в состав Заказа. Оплатить Заказ можно одним из
            способов, указанных в сообщении при оформлении Заказа, так же информация указана на сайте ipakyuli.com в
            соответствующих разделах.</p>
        <p>3.5. После получения оплаты за Товары, включенных в состав Заказа, <span>Продавец отправляет Товар к покупателю условленном с </span>Покупателемспособом
            (например: через почту, через посыльного, Покупатель сам подходит к Продавцу или другим способом на которой
            стороны договорятся).</p>
        <p>3.6. Если в течении 48 часов (два календарных дня) Клиент (Покупатель) не запрашивает информацию о
            соответствующем товаре, а так же не сообщает о том, что какие либо товары его не устраивают, то
            подтверждение Заказа выполняется системой автоматически, то есть заказ считается выполненным.</p>
        <p>3.7. После подтверждения всех Товарных позиций, содержащихся в Заказе, Товары упаковывают в посылку и
            отправляют по адресу (внутри Республики Узбекистан), указанному покупателем при оформлении заказа.</p>
        <p>3.8. После окончательного расчета Продавец организует отправку посылки Покупателю в соответствии с выбранным
            Покупателем способом доставки.</p>
        <p>3.9. Продавец не гарантирует постоянное присутствие выбранного Покупателем товара в каталоге товаров на
            ipakyuli.com<span>.</span> Покупатель будет проинформирован об отсутствии в каталоге
            Продавцом товара, оформленного на сайте ipakyuli.com.</p>
        <p>3.10. Перед заказом товара Покупатель обязуется ознакомиться с перечнем товаров, запрещенных к пересылке
            почтовыми службами страны Покупателя и с условиями оформления товаров, заказанных на ipakyuli.com<span
            >.</span></p>
        <p>3.11. Продавец самостоятельно несет ответственность за соответствие продаваемого им товара, условия доставки
            почтовой службы, включая, но не ограничиваясь габаритами, весом и содержанием товара, включая габариты и вес
            почтовой тары, необходимой для пересылки товара Покупателю Почтовой службой.</p>
        <p>3.12. В случае отсутствия заказанных Товаров на складе Продавца, Продавец вправе аннулировать указанный товар
            из Заказа Покупателя и уведомить об этом последнего путем направления электронного сообщения по адресу,
            указанному при оформлении заказа.</p>
        <p>3.13. В случае, если Продавец не соблюдает заявленные сроки поставки, вследствие чего задерживается
            выполнение заказов Покупателя, Исполнитель не несет никакой ответственности.</p>
        <p>3.14. В случае, если заказанный Товар не может быть поставлен в адрес, указанный Покупателем вследствие
            действующих ограничений на пересылку товаров почтовыми службами, Продавец вправе аннулировать товар из
            Заказа Покупателя и уведомить об этом последнего путем направления электронного сообщения по адресу,
            указанному при оформлении.</p>
        <p>3.15. В случае отказа Покупателя от заказа Продавец имеет право удержать с Покупателя стоимость заказанных
            Товаров и услуг по их доставке.</p>
        <p>3.16. Изменение способа или адреса доставки Товара возможно исключительно в том случае, если Товар в данный
            момент находиться в статусе &laquo;на складе&raquo;, если заказу уже присвоен любой из других статусов
            &ndash; изменение данной информации технически невозможно.</p>
        <p><strong>4. Оплата</strong></p>
        <p>4.1. Покупатель производит оплату способами, указанными в разделе &laquo;Оплата&raquo; на сайте
            ipakyuli.com</p>
        <p>4.2. Если фактическая цена товара будет отличена от цены, заявленной Продавцом при заказе товара Покупателем,
            Продавец при первой возможности информирует об этом Покупателя для подтверждения, либо аннулирования
            заказанного товара. При невозможности связаться с Покупателем данный заказ на покупку Товара считается
            аннулированным.</p>
        <p>4.3. Исполнитель не кредитует Покупателей, не выкупает товары в долг, не отправляет Посылки в долг, не
            контролирует Покупателя или Продавца, не отвечает за действия почты или доставщика.</p>
        <p><strong>5. Формирование Посылки и Доставка товаров</strong></p>
        <p>5.1. Покупатель самостоятельно выбирает способ доставки Товара. Доставка товаров осуществляется по тарифам и
            способами, указанным в разделе "Доставка" на сайте ipakyuli.com</p>
        <p>5.2. Покупатель обязуется предоставить Продавцу правильные и полные данные для осуществления доставки
            Товара.</p>
        <p>5.3. Оплата доставки товаров не включена в стоимость Товара и производится Покупателем отдельно от оплаты
            самих товаров. Окончательная стоимость доставки товаров до Покупателя указывается после того, как посылка
            упакована и взвешена. После упаковки посылки Продавец не вскрывает упакованные посылки и не меняет товары. В
            случае нехватки денежных средств на счету Покупателя, передача посылки в службу доставки будет задержана до
            погашения Покупателем долга на лицевом счету.</p>
        <p>5.4. Покупатель может застраховать посылку, указав страховую сумму при оформлении посылки. Страховая премия
            снимается с лицевого счета Покупателя. В случае пропажи посылки, Продавец пишет заявление в почтовую службу
            на возмещение страховой суммы. В случае возмещения, Продавец начисляет страховую сумму на счет
            Покупателя.</p>
        <p>5.5. Продавец обязуется передать упакованную посылку с товарами Покупателя почтовой службе. Услуги считаются
            полностью оказанными Продавцом с момента передачи товаров Покупателя почтовой службе.</p>
        <p>5.6. Исполнитель не отвечает за действия почты, курьерских служб и транспортных компаний и не может влиять на
            скорость доставки. Указанные на сайте ipakyuli.com сроки доставки являются приблизительными.</p>
        <p>5.7. Исполнитель не несёт ответственности перед Покупателем за порчу, утерю или гибель Товара во время
            доставки.</p>
        <p><strong>6. Обязанности Исполнителя</strong></p>
        <p>6.1. Исполнитель обязуется предоставить Покупателю Услуги, а также выполнять требования Покупателя, связанные
            с выполнением этих услуг, если данные требования не противоречат данному Договору, информации, указанной на
            сайте ipakyuli.com</p>
        <p><strong>7.</strong><strong>Обязанности Продавца</strong></p>
        <p>7.1. Продавец обязуется сохранять конфиденциальность предоставленных Покупателем данных и не разглашать их
            третьим лицам, если это не требуется для выполнения условий данного соглашения.</p>
        <p>7.2. Продавец обязуется предоставлять Покупателю информацию о выполнении поручений, указанных в данном
            Договоре.</p>
        <p>7.3. Продавец обязуется сообщать Покупателю информацию о выявленных явных дефектах при комплектации посылки
            Товара посредством внутренней системы сообщений ресурса ipakyuli.com</p>
        <p><strong>8. Обязанности Покупателя</strong></p>
        <p>8.1. Покупатель обязуется своевременно и в полном объеме предоставлять достоверную информацию, необходимую
            для оказания Услуг Исполнителем. В случае недостатка такой информации или сомнений в ее достоверности
            Исполнитель имеет право приостановить оказание Услуг.</p>
        <p>8.2. Покупатель обязуется своевременно и в полном объеме оплачивать стоимость заказанных товаров, стоимость
            их доставки до склада Продавца, стоимость доставки товара до Покупателя. В случае нехватки средств Продавец
            имеет право приостановить или прекратить обслуживание Покупателя.</p>
        <p>8.3. Покупатель обязуется проверять наличие и целостность товара при получении.</p>
        <p>8.4. Покупатель обязуется не привлекать Исполнителя ответчиком или соответчиком по любым обязательствам и
            расходам, связанным с ущербом, нанесенным Покупателю в результате действия третьих лиц, включая, но не
            ограничиваясь Продавцом, Почтовыми и курьерскими службами, платежными системами.</p>
        <p><strong>9. Условия возврата и обмена</strong></p>
        <p>9.1. В случае возврата/обмена стоимость доставки товара от Покупателя Продавцу, а так же в обратных
            направлениях, оплачивается Покупателем.</p>
        <p>9.2. Продавец принимает решение о возможности возврата/обмена товара.</p>
        <p>9.3. В случае, если Продавец принимает решение об отказе Покупателю в возврате/обмене, Исполнитель не
            компенсирует Покупателю стоимость Товара.</p>
        <p>9.4. В случае возврата/обмена Товаров, соответствующих требованиям в Заказе Покупателя, процедура
            возврата/обмена считается Дополнительной услугой и оплачивается Покупателем отдельно. Возврат/обмен Товара
            возможен, исключительно, при соблюдении следующих условий: Продавец согласен о возврате Товара, Товарная
            позиция еще не подтверждена Покупателем. При осуществлении процедуры возврата/обмена Товаров Покупателем так
            же оплачивается отдельно стоимость доставки Товара.</p>
        <p>9.5. Все операции по возврату/обмену осуществляются в полном соответствии с условиями Продавца, включая, но
            не ограничиваясь сроками, процедурой и размером компенсации.</p>
        <p>9.6. Покупатель должен учитывать сроки доставки Товара от Покупателя Продавцу, сроки обработки
            возврата/обмена на складе Продавца (до 5 рабочих дней).</p>
        <p><strong>10. Ограничение ответственности</strong></p>
        <p>10.1. Исполнитель не несет ответственности за любые расходы Покупателя или прямой либо косвенный ущерб,
            который может быть нанесен Покупателю вследствие использования услуг Исполнителя, причиненный Покупателю в
            результате использования или невозможности использования Услуг и понесенный в результате ошибок, пропусков,
            перерывов в работе, удаления файлов, изменения функций, дефектов, задержек в работе при передаче данных и
            т.п.</p>
        <p>10.2. Исполнитель не несет ответственности за действия смежных служб и сервисов, используемых для
            предоставления Услуг Покупателю, но не принадлежащих Исполнителю, а именно: банки, почтовые службы,
            интернет-провайдеры, эмейл-сервисы, платежные системы и т.д.</p>
        <p>10.3. Исполнитель не несет ответственности за сроки доставки товара от Продавца к Покупателю.</p>
        <p>10.4. Исполнитель не несет ответственности за качество, а также не обеспечивает гарантией товары,
            приобретенные Покупателем с помощью Услуг Исполнителя.</p>
        <p>10.5. Исполнитель не несет ответственность за несоответствие фактического размера, заявленного на сайте
            Продавца и размера Товара, если на ярлыках, этикетках, сопроводительных документах к товару указан размер,
            соответствующий заказу.</p>
        <p>10.6. При выявлении явных расхождений по комплектации, размерности и цвету Товара, Продавец проводит замену
            таких Товаров только в случае, если возврат и замена Товаров предусмотрена Продавцом. Покупатель обязуется
            самостоятельно отслеживать информацию о возможности замены Товара, предоставленную Продавцом, на сайте
            ipakyuli.com.</p>
        <p>10.7. Исполнитель не проверяет товар на соответствие заявленных Продавцом характеристик и в случае ошибки
            Продавца не несет ответственности за несоответствие товара.</p>
        <p>10.8. Исполнитель не несет ответственность за недобросовестность выбранных Покупателем Продавцов в случаях:
            товар выслан с задержкой; товар выслан, но не соответствует описанию; товар не рабочий (хотя заявлен как
            рабочий); товар вообще не выслан (случаи мошенничества).</p>
        <p>10.9. Исполнитель не является сотрудником транспортной компании или почтовой службы и не несет
            ответственность ни за один из способов пересылки товара и соблюдение сроков доставки товара.</p>
        <p>10.10. Исполнитель возмещает убытки Покупателя по его заявлению, и только в случае, если заявленные убытки
            явились результатом ошибок, допущенных служащими Исполнителя. При этом сумма возмещения не может превышать
            стоимости услуги Исполнителя.</p>
        <p><strong>11. Форс-мажор</strong></p>
        <p>11.1. Стороны освобождаются от ответственности за частичное или полное неисполнение обязательств по
            настоящему Договору, если это неисполнение явилось следствием обстоятельств непреодолимой силы, возникшей
            после заключения Договора в результате событий чрезвычайного характера, которые участник не мог ни
            предвидеть, ни предотвратить разумными мерами (форс-мажор). К таким событиям чрезвычайного характера
            относятся: наводнение, пожар, землетрясение, взрыв, шторм, эпидемия и иные явления природы, а также война
            или военные действия и т.п.</p>
        <p><strong>12. Разрешение споров</strong></p>
        <p>12.1. В случае возникновения разногласий и споров, связанных с выполнениями условий данного Договора стороны
            решают их путем переговоров.</p>
        <p>12.2. В случае не достижения согласия в ходе переговоров, споры по усмотрению истца будут разрешаться в одним
            из нижеперечисленных судебном порядке в соответствии с действующим законодательством Республики
            Узбекистан:</p>
        <p>а) разрешения споров с физическими лицами будут рассматриваться в Яккасарайском межрайонном Гражданском суде
            города Ташкента.</p>
        <p>б) разрешения споров с физическими или юридическими лицами будут рассматриваться в Третейском суде при
            Общественном фонде развития международных арбитражных и третейских судов Узбекистана.</p>
        <p>в) разрешения споров с юридическими лицами будут рассматриваться в Ташкентском межрайонном Экономическом
            суде.</p>
        <p><strong>13. Публикация информации на сайте</strong></p>
        <p>13.1. Размещение информации на сайте должно происходить в полном соответствии с Правилами размещения
            информации на сайте ipakyuli.com.</p>
        <p>Администрация сайта не осуществляет обязательную проверку всей размещаемой Покупателем или Продавцом
            информации. Проверка осуществляется выборочно и по усмотрению администрации.</p>
        <p>13.2. Администрация сайта сохраняет за собой право отказать в публикации размещенной пользователем
            информации, если опубликованная информация не соответствует Правилами размещения информации на сайте
            ipakyuli.com. При этом информация не будет опубликована как в учетной записи Продавца, так и в
            соответствующих разделах сайта.</p>
        <p><strong>14. Как Вы можете связаться с нами</strong></p>
        <p>14.1. Если у Вас возникли вопросы на счет данного Договора, пожалуйста, отправьте нам электронное сообщение
            на адрес&nbsp;<a href="mailto:index@ipakyuli.com"><u><span>index</span></u><u>@</u><u><span
                    >ipakyuli</span></u><u>.</u><u><span>com</span></u></a>.</p>
        <p>С условиями договора ознакомился, соглашаюсь с ними, принимаю их и обязуюсь выполнять.</p>
    </div>
    @include('partials.offer')
@endsection
