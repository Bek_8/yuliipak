@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="info-companies">
            <div class="info-companies__top company">
                <div class="company__left organization">
                    <div class="organization__logo">
                        <img src="{{asset('/img/logo-company.svg')}}" alt="company" class="organization__logo-img"/>
                    </div>
                    <div class="organization__info">
                        <div class="organization__name">OAO “Jasur Metalurg LTD”</div>
                        <div class="organization__info-item">
                            <div class="organization__info-left">Сферы работы</div>
                            <div class="organization__info-right">Ulugbek Alimov</div>
                        </div>
                        <div class="organization__info-item">
                            <div class="organization__info-left">Дата открытия</div>
                            <div class="organization__info-right">09.08.1990</div>
                        </div>
                        <div class="organization__info-item">
                            <div class="organization__info-left">Номер телефона</div>
                            <div class="organization__info-right">+99899 881 80 60</div>
                        </div>
                        <div class="organization__info-item">
                            <div class="organization__info-left">Банк</div>
                            <div class="organization__info-right">Ipateka Bank</div>
                        </div>
                    </div>
                </div>
                <div class="company__right">
                    <div class="company-info">
                        <div class="company-info__table">
                            <div>
                                <div class="company-info__table-top">Кол-во сотрудников</div>
                                <div>500 - 700</div>
                            </div>
                            <div>
                                <div class="company-info__table-top">Страна</div>
                                <div>50 - 70</div>
                            </div>
                            <div class="company-info__table-last">
                                <div class="company-info__table-top">Описание</div>
                                <div>Отсутствует</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-product">
                <div class="section-product-wrapper">
                    <div class="main-title">Ещё от производителя
                    </div>
                    <div class="container">
                        <div class="row d-flex flex-grow">
{{--                            @foreach($product->vendor->product as $product_like)--}}
{{--                                @if($product_like->id != $product->id)--}}
{{--                                    <a class="product"--}}
{{--                                       href="{{action('PageController@showProduct',[$product_like->vendor->alias,$product_like->alias])}}">--}}
{{--                                        <div class="product__top">--}}
{{--                                            <img src="{{ asset($product_like->image[0]) }}" alt="product">--}}
{{--                                        </div>--}}
{{--                                        <div class="product__bottom">--}}
{{--                                            <div class="title">{{$product_like->name}}</div>--}}
{{--                                            <div--}}
{{--                                                class="price">{{ count($product_like->range) > 1 ? number_format($product_like->range[0]).' - '.number_format($product_like->range[1]) : number_format($product_like->range[0])  }}--}}
{{--                                                UZS--}}
{{--                                            </div>--}}
{{--                                            <div class="count">--}}
{{--                                                <span class="gray">Мин. заказ:</span>--}}
{{--                                                <b class="bold">{{$product_like->min_count}}</b>--}}
{{--                                                <span class="gray">{{$product_like->count_type ?? 'шт'}}</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
