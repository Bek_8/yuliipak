@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="">
                    <div class="white-block bronItem mb-30">
                        <div class="head jv_head">
                            <div class="d-flex flex-row align-items-center justify-content-between">
                                <h3 class="title">Заказ №{{$data->id}}</h3>
                                <h3 class="title">Производитель <span
                                        class="badge badge-primary ml-5"> {{$data->vendor->name}}</span></h3>
                                <div class="btn btn-outline-primary">Сумма: {{number_format($data->price)}} UZS</div>
                            </div>
                        </div>
                        <ul class="list-block">
                            @foreach($data->order_item as $item)
                                <li class="item">
                                    <a class="left-block" style="flex:1;"
                                       href="{{ action('ProductController@index', [$data->vendor->alias]) }}">
                                        <i class="fe fe-align-center"></i>
                                        <p class="pl-2 d-flex align-items-center">
                                            <span>{{ $item->product->name_ru }}</span>
                                        </p>
                                    </a>
                                    <div class="right-block">
                                        <div class="btn btn-primary mr-5">{{'Кол-во :'.$item->amount .' * Цена :'.number_format($item->price)}}
                                            UZS
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
