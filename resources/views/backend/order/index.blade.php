@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="">
                    <div class="white-block bronItem mb-30">
                        <div class="head jv_head">
                            <h3>
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                     class="feather feather-calendar">
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                    <line x1="16" y1="2" x2="16" y2="6"></line>
                                    <line x1="8" y1="2" x2="8" y2="6"></line>
                                    <line x1="3" y1="10" x2="21" y2="10"></line>
                                </svg>
                                Заказы
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="datatable">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="jv_table_head_th">Дата
                                            <div class="arrows">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-up">
                                                    <polyline points="18 15 12 9 6 15"></polyline>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-down">
                                                    <polyline points="6 9 12 15 18 9"></polyline>
                                                </svg>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="jv_table_head_th"> Вендор
                                            <div class="arrows">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-up">
                                                    <polyline points="18 15 12 9 6 15"></polyline>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-down">
                                                    <polyline points="6 9 12 15 18 9"></polyline>
                                                </svg>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="jv_table_head_th"> Статус
                                            <div class="arrows">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-up">
                                                    <polyline points="18 15 12 9 6 15"></polyline>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-down">
                                                    <polyline points="6 9 12 15 18 9"></polyline>
                                                </svg>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="jv_table_head_th"> Цена
                                            <div class="arrows">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-up">
                                                    <polyline points="18 15 12 9 6 15"></polyline>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-down">
                                                    <polyline points="6 9 12 15 18 9"></polyline>
                                                </svg>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="jv_table_head_th"> Действия
                                            <div class="arrows">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-up">
                                                    <polyline points="18 15 12 9 6 15"></polyline>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-chevron-down">
                                                    <polyline points="6 9 12 15 18 9"></polyline>
                                                </svg>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $order)
                                    <tr>
                                        <td>{{ $order->created_at }}</td>
                                        <td>{{ $order->vendor->name }}</td>
                                        <td>
                                            @if($order->status == 0)
                                                В ожидании
                                            @endif
                                            @if($order->status == 1)
                                                Подтвержден
                                            @endif
                                            @if($order->status == 2)
                                                Доставлен
                                            @endif
                                        </td>
                                        <td>{{$order->price}} UZS</td>
                                        <td>
                                            <a href="{{action('OrderController@show',$order->id)}}"
                                               class="btn btn-primary">Подробности</a>
                                            <a href="{{action('MessageController@show',$order->id)}}"
                                               class="btn btn-primary">Сообщения</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <ul class="jv_status_info">
                                <li>Доставлен</li>
                                <li>В Ожидании</li>
                                <li>Подтвержден</li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables/datatables.css')}}">
@endsection
@section('script')
    <script>
        let __origDefine = define;
        define = null;
    </script>
    <script src="{{ asset('backend/plugins/fullcalendar/js/moment.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/plugins/filer/jquery.filer.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $('.datatable').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": "предыдущие",
                    "sNext": "далее",
                }
            }
        });
    </script>
@endsection
