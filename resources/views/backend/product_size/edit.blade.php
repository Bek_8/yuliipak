@extends('layouts.backend')

@section('content')
<section>
<div class="container">
<div class="row">
    <form action="{{ action('ProductSizeController@update',[$vendor->alias,$product->alias,$data->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="white-block mb-30">
          <div class="head">
              <h3>Изменить</h3>
          </div>
          <div class="content">
            <div class="input-block">
                <div class="input">
                    <label for="name">Название (RU)</label>
                    <input required="required" type="text" name="name" value="{{$data->name}}" class="form-control regStepOne" id="name" />
                </div>
            </div>
          </div>
        </div>
        <div class="white-block mb-30">
        <div class="head">
            <h3>Общие фотографии</h3>
        </div>
        <div class="card-header-after">
          <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин. размер изображений 500x500 пикс.</p>
          <div class="uploader">
                <ul class="jFiler-items-list jFiler-items-grid">
                    @foreach($images as $image)
                    <li class="gallryUploadBlock_item photo-thumbler d-inline-block" data-image="{{basename($image)}}">
                        <div class="jFiler-item-thumb-image">
                            <img src="{{asset($image)}}" alt="image">
                        </div>
                    <div class="removeItem">
                        <span class="deletePhoto" data-image="{{basename($image)}}">
                            <i class="fe fe-minus"></i>
                        </span>
                    </div>
                    </li>
                    @endforeach
                </ul>
              <input type="file" name="image[]" class="filer_input3" multiple="multiple">
          </div>
        </div>
        </div>
        <div class="button-block">
            <button type="submit" class="continue-btn">Сохранить</button>
        </div>
    </form>
</div>
</div>
</section>
@endsection
@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
<script>
$(document).ready(function() {
    $(".filer_input3").filer({
        limit: null,
        maxSize: null,
        extensions: null,
        changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
            item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
            <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
            </li>`,
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: true,
            canvasImage: true,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.fe.fe-minus'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
            dragContainer: null,
        },
        files: null,
        addMore: true,
        allowDuplicates: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        itemAppendToEnd: true,
        onSelect: null,
        afterShow: function (jqEl, htmlEl, parentEl, itemEl) {
            $('.galleryAddElement').hide()
            $('.cloneGalleryAddElement').remove()
            $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
        },
        onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
            let filerKit = inputEl.prop("jFiler"),
                file_name = filerKit.files_list[id].name;
            if (filerKit.files_list.length == 1) {
                $('.galleryAddElement').show()
            }
        },
        onEmpty: null,
        options: null,
    });
    $('.uploader').on('click', '.cloneGalleryAddElement', function (e) {
        $('.galleryAddElement').trigger('click');
    });
    $('.deletePhoto').click((e) => {
        let removeImage = $(e.target);
        let imageData = removeImage.data('image');
        if(confirm('Вы уверены?')){
        let deleteItem = $.get("{{action('ProductSizeController@removeImage',$data->id)}}", {file_name: imageData});
            deleteItem.done(() => {
                removeImage.parent().parent().remove();
            });
        }
    });
});
</script>
@endsection
