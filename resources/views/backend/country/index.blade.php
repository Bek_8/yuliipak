@extends('layouts.backend')

@section('content')
<section>
    <div class="container">
        <h2 class="blue-title">Страны</h2>
        <div class="row">
            <form action="#" autocomplete="off">
                <div class="white-block hotels">
                    <ul class="list-block">
                        @foreach($data as $datas)
                        <li class="item">
                            <a class="left-block" href="{{ action('CityController@index' , $datas->id) }}">
                                <i class="flag flag-{{$datas->flag}}"></i>
                                <p class="pl-2">{{ $datas->name_ru  }}</p>
                            </a>
                            <div class="right-block">
                                <a href="{{ action('CityController@index' , $datas->id) }}" class="btn btn-primary mr-2">
                                    <i class="fe fe-arrow-right"></i>
                                </a>
                                <a href="{{ action('CountryController@edit' , $datas->id) }}" class="btn btn-primary">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </div>
                        </li>
                        @endforeach
                        <a class="add-list-btn" href="{{ action('CountryController@create') }}">
                            <i class="fe fe-plus-circle"></i>
                            Добавить
                        </a>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
