@extends('layouts.backend')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <form action="{{ action('SettingController@update',$data->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="white-block mb-30">
                  <div class="head">
                      <h3>Изменить</h3>
                  </div>
                  <div class="content">
                    <div class="input-block">
                        <div class="input">
                            <label for="title">Название (RU)</label>
                            <input required="required" type="text" name="title" value="{{$data->title}}" class="form-control regStepOne" id="title" />
                        </div>
                    </div>
                    <div class="input-block">
                        <div class="input">
                            <label for="description">Описание (RU)</label>
                            <textarea name="description" id="description" class="form-text">{{$data->description}}</textarea>
                        </div>
                    </div>
                  <div class="input-block">
                    <div class="input">
                        <label>Фото:</label>
                        <input type="file" name="image" class="image">
                        <p><h4>Загрузите для обновления фотографии</h4></p>
                        <div class="d-flex justify-content-between mt-5">
                            @foreach($images as $image)
                                <img src="{{asset($image)}}?time={{microtime(true)}}" alt="image" width="150" />
                            @endforeach
                        </div>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="button-block">
                    <button type="submit" class="continue-btn">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
