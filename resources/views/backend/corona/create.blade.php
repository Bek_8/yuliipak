@extends('layouts.backend')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <form action="{{ action('SettingController@store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="white-block mb-30">
                  <div class="head">
                      <h3>Добавить</h3>
                  </div>
                  <div class="content">
                    <div class="input-block">
                        <div class="input">
                            <label for="title">Название (RU)</label>
                            <input required="required" type="text" name="title" value="{{old('title')}}" class="form-control regStepOne" id="title" />
                        </div>
                    </div>
                    <div class="input-block">
                        <div class="input">
                            <label for="description">Описание (RU)</label>
                            <textarea name="description" id="description" class="form-text">{{old('description')}}</textarea>
                        </div>
                    </div>
                  <div class="input-block">
                    <div class="input">
                        <label>Фото:</label>
                        <input type="file" required="required" name="image[]">
                    </div>
                  </div>
                  </div>
                </div>
                <div class="button-block">
                    <button type="submit" class="continue-btn">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
