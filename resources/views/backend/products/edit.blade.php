@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('ProductController@update',[$vendor->alias,$data->alias]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить продукт</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{$data->name_ru}}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{$data->name_uz}}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <textarea name="description_ru" class="form-control regStepOne text-area"
                                              id="description_ru" cols="30"
                                              rows="10">{{$data->description_ru}}</textarea>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <textarea name="description_uz" class="form-control regStepOne text-area"
                                              id="description_uz" cols="30"
                                              rows="10">{{$data->description_uz}}</textarea>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select name="category_id" id="category" class="chosen-select form-control">
                                        <optgroup label="Главные">
                                            @foreach( $categories as $datas )
                                                <option value="{{ $datas->id }}"
                                                        @if($datas->id == $data->category_id) selected @endif>{{ $datas->name_ru }}</option>
                                            @endforeach
                                        </optgroup>
                                        @foreach( $categories as $datas )
                                            <optgroup label="{{$datas->name_ru}}">
                                                @foreach($datas->sub_category as $sub)
                                                    <option value="{{ $sub->id }}"
                                                            @if($sub->id == $data->category_id) selected @endif>{{ $sub->name_ru }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30 changable" id="badsBlock">
                        <div class="head">
                            <h3>Цены</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="price_dropper">Скидка</label>
                                    <input required="required" type="text" name="price_dropper"
                                           value="{{$data->price_dropper}}" class="form-control regStepOne"
                                           id="price_dropper"/>
                                </div>
                                    <div class="input">
                                        <label for="sizes">Размер (м3)</label>
                                        <input required="required" type="text" name="sizes" value="{{$data->sizes}}"
                                               class="form-control regStepOne" id="sizes" placeholder=""/>
                                    </div>
                                    <div class="input">
                                        <label for="weight">Вес (кг)</label>
                                        <input required="required" type="text" name="weight" value="{{$data->weight}}"
                                               class="form-control regStepOne" id="weight" placeholder=""/>
                                    </div>
                                <div class="input">
                                    <label for="min_count">Минимальное кол-во</label>
                                    <input required="required" type="text" name="min_count" value="{{$data->min_count}}"
                                           class="form-control regStepOne" id="min_count"/>
                                </div>
                                <div class="input">
                                    <label for="count_type">Тип измерения (шт\кг\комплект)</label>
                                    <input required="required" type="text" name="count_type"
                                           value="{{$data->count_type}}" class="form-control regStepOne"
                                           id="count_type"/>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            @foreach(json_decode($data->price,true) as $key => $price)
                                <div class="input-block with-trash">
                                    <div class="input">
                                        <div class="half-block max-992 mobile-mb-30">
                                            <p>Количество ({{$data->count_type}})</p>
                                            <input required="required" type="number" value="{{$price[0]}}"
                                                   name="amount[]" class="form-control" min="1"/>
                                        </div>
                                        <div class="half-block max-992">
                                            <p class="second">Цена ({{$data->count_type}})</p>
                                            <input required="required" type="text" value="{{$price[1]}}"
                                                   name="pricing[]" class="form-control" min="0"/>
                                        </div>
                                        <div class="half-block max-992">
                                            <p class="second">Кол-во дней на производство</p>
                                            <input required="required" type="number" value="{{$price[2] ?? 1}}"
                                                   name="produce[]" class="form-control" min="1"/>
                                        </div>
                                    </div>
                                    @if($key != 0)
                                        <div class="text-block">
                                            <a href="#" class="btnRemovePrice"><i class="fa-lg fe fe-trash-2"></i></a>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                            <div class="input-block with-trash text-block">
                                <span class="add-button d-flex align-items-center" style="height: 42px;"
                                      id="btnAddprice"><i
                                        class="fe fe-plus-circle"></i> <span>Добавить цену</span></span>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="currency">Выберите валюту</label>
                                    <select name="currency" id="currency" class="chosen-select form-control">
                                        <option value="usd" @if($data->currency == 'usd') selected @endif>USD</option>
                                        <option value="uzs" @if($data->currency == 'uzs') selected @endif>UZS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин.
                                размер изображений 500x500 пикс.</p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($images as $image)
                                        <li class="gallryUploadBlock_item photo-thumbler d-inline-block"
                                            data-image="{{basename($image)}}">
                                            <div class="jFiler-item-thumb-image">
                                                <img src="{{asset($image)}}" alt="image">
                                            </div>
                                            <div class="removeItem">
                        <span class="deletePhoto" data-image="{{basename($image)}}">
                            <i class="fe fe-minus"></i>
                        </span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <input type="file" name="image[]" class="filer_input3" multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                        <a href="{{ url()->current() }}" class="blue-text ml-40">Отменить изменения</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script
        src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea.text-area',
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '{{ asset('css/styles.css') }}'
            ]
        });
        $(document).ready(function () {
            $(".filer_input3").filer({
                limit: null,
                maxSize: null,
                extensions: null,
                changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
                showThumbs: true,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
                    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
                    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
                    </li>`,
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: true,
                    canvasImage: true,
                    removeConfirmation: false,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.fe.fe-minus'
                    }
                },
                dragDrop: {
                    dragEnter: null,
                    dragLeave: null,
                    drop: null,
                    dragContainer: null,
                },
                files: null,
                addMore: true,
                allowDuplicates: false,
                clipBoardPaste: true,
                excludeName: null,
                beforeRender: null,
                afterRender: null,
                beforeShow: null,
                beforeSelect: null,
                itemAppendToEnd: true,
                onSelect: null,
                afterShow: function (jqEl, htmlEl, parentEl, itemEl) {
                    $('.galleryAddElement').hide()
                    $('.cloneGalleryAddElement').remove()
                    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
                },
                onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                    let filerKit = inputEl.prop("jFiler"),
                        file_name = filerKit.files_list[id].name;
                    if (filerKit.files_list.length == 1) {
                        $('.galleryAddElement').show()
                    }
                },
                onEmpty: null,
                options: null,
            });
            $('.uploader').on('click', '.cloneGalleryAddElement', function (e) {
                $('.galleryAddElement').trigger('click');
            });
            $('.deletePhoto').click((e) => {
                let removeImage = $(e.target);
                let imageData = removeImage.data('image');
                if (confirm('Вы уверены?')) {
                    let deleteItem = $.get("{{action('ProductController@removeImage',$data->id)}}", {file_name: imageData});
                    deleteItem.done(() => {
                        removeImage.parent().parent().remove();
                    });
                }
            });
        });
        let btnAddprice = $('#btnAddprice');
        let btnRemovePrice = $('.btnRemovePrice');
        let priceHtml = `<div class="input-block with-trash">
                    <div class="input">
                        <div class="half-block max-992 mobile-mb-30">
                            <p>Количество ({{$data->count_type}})</p>
                            <div class="d-inline-block selectize-control">
                                <input required="required" type="number" value="1" name="amount[]" class="form-control" min="1" />
                            </div>
                        </div>
                        <div class="half-block max-992">
                            <p>Цена ({{$data->count_type}})</p>
                            <input required=required" type="number" name="pricing[]" min="0" value="0">
                        </div>
                        <div class="half-block max-992">
                            <p class="second">Кол-во дней на производство</p>
                            <input required="required" type="number" value="1" name="produce[]" class="form-control" min="1" />
                        </div>
                    </div>
                    <div class="text-block">
                        <a href="#" class="btnRemovePrice"><i class="fa-lg fe fe-trash-2"></i></a>
                    </div>
                    </div>`;
        btnAddprice.on('click', function () {
            let count = $('.with-trash').length;
            if (count <= 10) {
                $(this).parent().before(priceHtml)
            } else {
                alert('Лимит цены привышен');
            }
        });
        $('#badsBlock').on('click', '.btnRemovePrice', function (e) {
            e.preventDefault();
            $(this).closest('.with-trash').remove();
        })
    </script>
@endsection
