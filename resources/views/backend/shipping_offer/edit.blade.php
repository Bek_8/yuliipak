@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('ShippingOfferController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label>Доставшик</label>
                                    <select name="carrier_id" class="form-control" id="select-beast">
                                        @foreach( $carriers as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if($datas->carrier_id === $datas->id)  selected @endif>{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{$data->name}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена</label>
                                    <input required="required" type="number" name="price" value="{{$data->price}}"
                                           class="form-control regStepOne" id="price" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="size">Размер (м3)</label>
                                    <input required="required" type="number" name="size" value="{{$data->size}}"
                                           class="form-control regStepOne" id="size" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="weight">Вес (кг)</label>
                                    <input required="required" type="number" name="weight" value="{{$data->weight}}"
                                           class="form-control regStepOne" id="weight" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
@endsection
