@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('ShippingController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label>Из (Регион)</label>
                                    <select name="from_id" class="form-control" id="select-beast">
                                        @foreach( $city as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="select-beast1">Конечный (Регион)</label>
                                    <select name="to_id" class="form-control" id="select-beast1">
                                        @foreach( $city as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
@endsection
