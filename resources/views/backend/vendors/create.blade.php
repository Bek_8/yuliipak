@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('VendorController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить прозводителя</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{old('name')}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{old('description_uz')}}" class="form-control regStepOne"
                                           id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{old('description_ru')}}" class="form-control regStepOne"
                                           id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Страна:</label>
                                    <select name="country_id" id="select-countries" class="form-control custom-select">
                                        @foreach( $country as $datas )
                                            <option data-data='{"image": "/backend/images/flags/{{$datas->flag}}.svg"}'
                                                    value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Регион</label>
                                    <select name="city_id" class="form-control" id="select-beast">
                                        @foreach( $city as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="address">Адрес</label>
                                    <input required="required" type="text" value="{{old('address')}}" class="regStepTwo"
                                           name="address" id="addressHotel"/>
                                </div>
                                <div class="text-block max-992">
                                    <p>укажите адрес, используя латинский алфавит</p>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="bank">Банк</label>
                                    <input required="required" type="text" value="{{old('bank')}}" class="regStepTwo"
                                           name="bank" id="addressHotel"/>
                                </div>
                                <div class="text-block max-992">
                                    <p>укажите адрес, используя латинский алфавит</p>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="bank_account">Р/С</label>
                                    <input required="required" type="text" value="{{old('bank_account')}}"
                                           class="regStepTwo" name="bank_account" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="inn">ИНН</label>
                                    <input required="required" type="text" value="{{old('inn')}}" class="regStepTwo"
                                           name="inn" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="out_account">В/С</label>
                                    <input required="required" type="text" value="{{old('out_account')}}"
                                           class="regStepTwo" name="out_account" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="swift">СВИФТ</label>
                                    <input required="required" type="text" value="{{old('swift')}}" class="regStepTwo"
                                           name="swift" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="mfo">МФО</label>
                                    <input required="required" type="text" value="{{old('mfo')}}" class="regStepTwo"
                                           name="mfo" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="oked">ОКЭД</label>
                                    <input required="required" type="text" value="{{old('oked')}}" class="regStepTwo"
                                           name="oked" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="nds">НДС Код</label>
                                    <input required="required" type="text" value="{{old('nds')}}" class="regStepTwo"
                                           name="nds" id="addressHotel"/>
                                </div>
                            </div>
                            <h2>Банк корреспонденты</h2>
                            <div class="input-block">
                                <div class="input">
                                    <label for="additional_bank">Банк</label>
                                    <input required="required" type="text" value="{{old('additional_bank')}}"
                                           class="regStepTwo" name="additional_bank" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="additional_swift">СВИФТ</label>
                                    <input required="required" type="text" value="{{old('additional_swift')}}"
                                           class="regStepTwo" name="additional_swift" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="bank_no">№ счета</label>
                                    <input required="required" type="text" value="{{old('bank_no')}}" class="regStepTwo"
                                           name="bank_no" id="addressHotel"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select multiple name="category_id[]" required id="category"
                                            class="chosen-select form-control">
                                        <optgroup label="Главные">
                                            @foreach( $categories as $datas )
                                                <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                            @endforeach
                                        </optgroup>
                                        @foreach( $categories as $datas )
                                            <optgroup label="{{$datas->name_ru}}">
                                                @foreach($datas->sub_category as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->name_ru }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if(Auth::user()->hasRole('admin'))
                                <div class="input-block">
                                    <div class="input">
                                        <label for="manager">Менеджер</label>
                                        <select name="user_id" class="form-control" id="manager">
                                            @foreach( $user as $key => $datas )
                                                @if($datas->hasRole('manager'))
                                                    <option value="{{ $datas->id }}">
                                                        {{ $datas->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="input-block">
                                <div class="input">
                                    <label for="service-phone">Телефон отдела обслуживания </label>
                                    <div class="phone">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-phone phone-icon">
                                            <path
                                                d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                        </svg>
                                        <input required="required" type="tel" class="regStepTwo forPhone" name="phone"
                                               value="{{old('phone')}}" id="phoneHotel">
                                    </div>
                                </div>
                                <div class="text-block max-992">
                                    <p>с кодом страны, города или оператора</p>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="open_date">Дата открытия</label>
                                    <input required="required" type="date" name="open_date" value="{{old('open_date')}}"
                                           class="form-control regStepOne" id="open_date" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="workers">Кол-во работников</label>
                                    <input required="required" type="number" name="workers" value="{{old('workers')}}"
                                           class="form-control regStepOne" id="workers" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин.
                                размер изображений 500x500 пикс.</p>
                            <div class="fotoUploader">
                                <input required="required" type="file" name="image[]" class="filer_input2"
                                       multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(function () {
            $('#category').chosen();
        });
        let latitude = $('#lat');
        let longitude = $('#long');

        function initMap() {
            let latlng = new google.maps.LatLng(41.311081, 69.240562);
            let map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 12,
                animation: google.maps.Animation.BOUNCE
            });
            let marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true
            });
            let lat, long;
            google.maps.event.addListener(marker, 'dragend', function (event) {
                lat = this.getPosition().lat().toFixed(6);
                long = this.getPosition().lng().toFixed(6);
                latitude.val(lat);
                longitude.val(long);
            });
        }
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
