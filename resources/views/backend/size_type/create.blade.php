@extends('layouts.backend')

@section('content')
<section>
<div class="container">
<div class="row">
    <form action="{{ action('ProductSizeTypeController@store',[$vendor->alias,$product->alias,$product_size->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="white-block mb-30">
          <div class="head">
              <h3>Добавить</h3>
          </div>
          <div class="content">
            <div class="input-block">
                <div class="input">
                    <label for="name_ru">Название (RU)</label>
                    <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}" class="form-control regStepOne" id="name_ru" />
                </div>
            </div>
            <div class="input-block">
                <div class="input">
                    <label for="name_uz">Название (RU)</label>
                    <input required="required" type="text" name="name_uz" value="{{old('name_uz')}}" class="form-control regStepOne" id="name_uz" />
                </div>
            </div>
          </div>
        </div>
        <div class="white-block mb-30 changable" id="badsBlock">
        <div class="head">
            <h3>Цены</h3>
        </div>
            <div class="content">
                <div class="input-block with-trash">
                    <div class="input">
                        <div class="half-block max-992 mobile-mb-30">
                            <p>Количество</p>
                            <input required="required" type="number" value="1" name="amount[]" class="form-control" min="1" />
                        </div>
                        <div class="half-block max-992">
                            <p class="second">Цена</p>
                            <input required="required" type="number" value="1" name="pricing[]" class="form-control" min="0" />
                        </div>
                        <div class="half-block max-992">
                            <p class="second">Кол-во дней на производство</p>
                            <input required="required" type="number" value="1" name="produce[]" class="form-control" min="1" />
                        </div>
                    </div>
                </div>
                <div class="input-block with-trash text-block">
                    <span class="add-button d-flex align-items-center" style="height: 42px;" id="btnAddprice"><i class="fe fe-plus-circle"></i> <span>Добавить цену</span></span>
                </div>
                  <div class="input-block">
                    <div class="input">
                        <label for="currency">Выберите валюту</label>
                        <select name="currency" id="currency" class="chosen-select form-control">
                            <option value="usd">USD</option>
                            <option value="uzs">UZS</option>
                        </select>
                    </div>
                  </div>
            </div>
        </div>
        <div class="white-block mb-30">
        <div class="head">
            <h3>Общие фотографии</h3>
        </div>
        <div class="card-header-after">
          <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин. размер изображений 500x500 пикс.</p>
          <div class="fotoUploader">
              <input type="file" name="image[]" class="filer_input2" multiple="multiple">
          </div>
        </div>
        </div>
        <div class="button-block">
            <button type="submit" class="continue-btn">Сохранить</button>
        </div>
    </form>
</div>
</div>
</section>
@endsection

@section('script')
<script>
let btnAddprice = $('#btnAddprice');
let btnRemovePrice = $('.btnRemovePrice');
let priceHtml = `<div class="input-block with-trash">
                    <div class="input">
                        <div class="half-block max-992 mobile-mb-30">
                            <p>Количество</p>
                            <div class="d-inline-block selectize-control">
                                <input required="required" type="number" value="1" name="amount[]" class="form-control" min="1" />
                            </div>
                        </div>
                        <div class="half-block max-992">
                            <p>Цена</p>
                            <input required=required" type="number" name="pricing[]" min="1" value="1">
                        </div>
                        <div class="half-block max-992">
                            <p class="second">Кол-во дней на производство</p>
                            <input required="required" type="number" value="1" name="produce[]" class="form-control" min="1" />
                        </div>
                    </div>
                    <div class="text-block">
                        <a href="#" class="btnRemovePrice"><i class="fa-lg fe fe-trash-2"></i></a>
                    </div>
                    </div>`;
    btnAddprice.on('click', function() {
    let count = $('.with-trash').length;
        if(count <= 10){
          $(this).parent().before(priceHtml)
        }
        else{
            alert('Лимит цены привышен');
        }
    });
$('#badsBlock').on('click', '.btnRemovePrice', function(e) {
  e.preventDefault();
  $(this).closest('.with-trash').remove();
})
</script>
@endsection
