@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Пользователи</h3>
                <div>
                    @isset($carrier)
                        <a class="btn btn-outline-primary" href="{{ action('ManagerController@createCarrier') }}">Добавить
                            Курьера</a>
                    @else
                        <a class="btn btn-outline-success" href="{{ action('ManagerController@create') }}">Добавить</a>
                    @endisset
                </div>
            </div>
            @if(count($data) > 0)
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Имя</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            {{--                @if($data[0]->roles[0]->type == 'member')--}}
                            {{--                    <th>Дата рег</th>--}}
                            {{--                @endif--}}
                            {{--                @if($data[0]->roles[0]->type !== 'member' && $data[0]->roles[0]->type !== 'callcenter')--}}
                            {{--                <th>Производитель</th>--}}
                            {{--                @endif--}}
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $datas)
                            <tr>
                                <td>{{ $datas->id }}</td>
                                <td>{{ $datas->name }}</td>
                                {{--                @if($data[0]->roles[0]->type == 'member')--}}
                                <td>{{ $datas->phone }}</td>
                                {{--                @endif--}}
                                <td>{{ $datas->email }}</td>
                                {{--                <td>--}}
                                {{--                    @if( $datas->roles[0]->type == 'admin' ) Модератор @endif--}}
                                {{--                    @if($datas->roles[0]->type == 'manager') Менеджер @endif--}}
                                {{--                    @if($datas->roles[0]->type == 'member') Гость @endif--}}
                                {{--                    @if($datas->roles[0]->type == 'callcenter') Оператор @endif--}}
                                {{--                </td>--}}
                                {{--                @if($datas->roles[0]->type !== 'member' && $data[0]->roles[0]->type !== 'callcenter')--}}
                                {{--                    @if($datas->vendor)--}}
                                {{--                        <td>{{$datas->vendor->name}}</td>--}}
                                {{--                    @else--}}
                                {{--                        <td>Нет</td>--}}
                                {{--                    @endif--}}
                                {{--                @endif--}}
                                <td class="text-right">
                                    <a href="{{ action('ManagerController@edit' , $datas->id) }}"
                                       class="btn icon border-0"><i class="fe fe-edit"></i></a>
                                    <form class="d-inline-block"
                                          action="{{ action('ManagerController@delete' , $datas->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn icon border-0"
                                                onclick="return confirm('Вы уверены?')">
                                            <i class="fe fe-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
@endsection
