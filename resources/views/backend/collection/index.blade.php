@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Коллекции</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $item)
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                <span>
                                    <i class="fe fe-list"></i> {{ $item->name_ru }}
                                </span>
                                        <div class="d-flex">
                                            <div class="d-flex">
                                                <a href="{{ action('CollectionController@edit',$item->id) }}"
                                                   class="btn btn-outline-primary">Изменить</a>
                                            </div>
                                            <form class="d-inline-block"
                                                  action="{{ action('CollectionController@delete' , $item->id) }}"
                                                  method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn icon border-0"
                                                        onclick="return confirm('Вы уверены?')">
                                                    <i class="fe fe-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('CollectionController@create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                        <div class="d-flex align-items-center justify-content-end">
                            {{ $data->links() }}
                        </div>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
