<?php

return [
    'nav' => [
        'Buyurtma haqida', 'Ommaviy oferta', 'Qidiruv', 'Biz xaqimizda', 'Joylashtirish shartlari', 'Savollar uchun',
        "Bo'limlar", "Barcha bo'limlar", 'Valyuta', 'Shaxsiy', 'Kabinet', "Ro'yxatdan o'tish", 'Kirish', 'Buyurtmalar',
        'Malumotlar',
        'Xatlar', 'Chiqish',
        'Savatcha', '', "Barcha bo'limlar", 'Mahsulot nomi'
    ],
    'index' => [
        'Все', 'Min.'
    ],
    'process' => [
        'Buyurtma raqami', 'Mahsulotlar', 'Holat', 'Shartnoma', 'Yopiq', 'Kutish jarayonida',
        "Iltimos, sotib olishdan oldin platformaning SMS xizmati orqali tovarlarni sotuvchisi bilan bog'laning!"
        , 'Tasdiqlandi', 'Yetkazib beruvchi tanlandi', "Yo'lda", 'Yetkazildi', 'Tasdiqlaysizmi',
        'Buyurtmani tasdiqlang', 'Muhokama', 'Tanlovlar', 'Tanlanmagan', 'Ishonchingiz komilmi',
        'Yetkazib beruvchini tanlang', 'Molni oldingizmi',
        'Tasdiqlash', 'Muvofaqiyatli'
    ],
    'messages' => [
        "Xatlar ro'yhati", "Xatlar ro'yxatiga qaytish", 'Tasdiqlangan narx', 'Xabar matni'
    ],
    'basket' => [
        'Muvaffaqiyatli o‘chirildi', 'Qadam 1 из 3', 'Yetkazib berish manzili', 'Tanlang', 'Tanlanmagan',
        'Qadam 2 из 3', "To'lov uslubi",
        "Bank o'tkazmasi",
        " Bank o'tkazmasi orqali to'lovni amalga oshirish xizmati to'lovning maxfiyligi va xavfsizligi tamoyillariga </br> asoslangan xalqaro bank tizimlarining qoidalari asoslanib amalga oshiriladi",
        'Qadam 3 из 3', 'Xarid savati', "Savatingiz bo'sh", 'Buyurtma narxi', 'Tovarlar uchun', "Tolov",
        'Amalga oshirish'

    ],
    'show' => [
        'Ushbu mahsulot uchun eng kam miqdori', 'QQS hisobga olingan holda', 'Ha', 'Mamlakat', 'QQS', "Qo'shilgan", "yo'q",
        'Mamlakat', 'Miqdor', 'Narx', 'Ishlab chiqarish', 'Kelishilgan holda', 'kun', 'Miqdori', 'Parametr', 'Turi',
        'Narx', 'Chegirma', 'Jami', "Savatchaga qo'shish", 'Tanlang', 'Tanlanmagan', 'Narxni kelishish',
        "Mahsulot haqida asosiy ma'lumotlar", 'Ishchilar soni', 'Ochilgan sana', 'Telefon', 'Ishlab chiqaruvchidan'
    ],
    'register' => [
        "Shaxsiy ma'lumotlar", 'Ismingiz', 'Telefon',
        'Saytdan buyurtmalar haqida SMS-xabar olasiz', "Parol o'ylab toping", 'Qayta kiriting',
        'Saqlash'
    ],
    'auth' => [
        "Shaxsiy ma'lumotlar", 'Ismingiz', 'Email manzil', 'Telefon', 'Shaxar', 'Mamlakat', 'Profil', 'Parol', 'Tasdiqlash',
        'Saqlash'
    ],
    'footer' => ['Tafsilotlar'],
];
