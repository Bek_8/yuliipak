<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('user_id');
            $table->string('name');
            $table->string('alias')->nullable();
            $table->longText('description_uz');
            $table->longText('description_ru');
            $table->string('address');
            $table->string('phone');
            $table->date('open_date');
            $table->boolean('enabled')->default(0);
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->mediumText('bank')->nullable();
            $table->mediumText('bank_account')->nullable();
            $table->mediumText('inn')->nullable();
            $table->mediumText('out_account')->nullable();
            $table->mediumText('swift')->nullable();
            $table->mediumText('mfo')->nullable();
            $table->mediumText('oked')->nullable();
            $table->mediumText('nds')->nullable();
            $table->mediumText('additional_bank')->nullable();
            $table->mediumText('additional_swift')->nullable();
            $table->mediumText('bank_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
