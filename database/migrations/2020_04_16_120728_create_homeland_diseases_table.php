<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomelandDiseasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeland_diseases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('homeland_id');
            $table->unsignedInteger('confirmed');
            $table->unsignedInteger('deaths');
            $table->unsignedInteger('recovered');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homeland_diseases');
    }
}
