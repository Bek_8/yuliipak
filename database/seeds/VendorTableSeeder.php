<?php

use Illuminate\Database\Seeder;

class VendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendor = \App\Vendor::create([
            'name'=>'Apple',
            'description_ru'=>'Apple',
            'description_uz'=>'Apple',
            'user_id'=>2,
            'city_id'=>1,
            'country_id'=>1,
            'address'=>'Cupertino, CA 95014',
            'phone'=> 998990008991,
            'open_date'=> Carbon\Carbon::now(),
            'lat'=> 41.311081,
            'long'=> 69.240562,
            'bank'=> 'Ipateka Bank',
            'bank_account'=> '29896000200000440104',
            'inn'=> '200833707',
            'out_account'=> '16103000200000440001',
            'swift'=> 'UZHOUZ 22',
            'mfo'=> '00440',
            'oked'=> '64190',
            'nds'=> '64190',
            'additional_bank'=> 'Ipak yuli Bank',
            'additional_swift'=> '64190',
            'bank_no'=> '16103000200000440001'
        ]);
        $vendor->categories()->attach([1]);
    }
}
