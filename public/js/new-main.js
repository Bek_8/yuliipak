$(document).ready(function(){
$(".main-card").slick({
  slidesToShow: 5,
  variableWidth: true,
  arrows: false,
  slidesToScroll: 1,
  swipeToSlide: true,
  integer: 1,
  initialSlide: 1,
  infinite: true,
  responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        swipeToSlide: true,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        swipeToSlide: true
      }
    },
    {
      breakpoint: 6,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true
      }
    }
  ]
});
$(".big-cards").slick({
  slidesToShow: 3,
  infinite: true,
  slidesToScroll: 1,
  swipeToSlide: true,
  dots: false,
  arrows: false,
  variableWidth: true,
  responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        swipeToSlide: true,
        infinite: true
      }
    },
    {
      breakpoint: 769,
      infinite: true,
      dots: false,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.slider').slick({
  autoplay: true,
  speed: 800,
  lazyLoad: 'progressive',
  arrows: true,
  dots: true,
  nextArrow: '<div class="slick-arrow next-arrow"><img src="/img/next_arrow.svg" alt="" class=""></div>',
  prevArrow: '<div class="slick-arrow prev-arrow"><img src="/img/prev_arrow.svg" alt="" class=""></div>'
}).slickAnimation();

$(".login-btn").click(function() {
    $(this).parent().find('.login-hidden').toggleClass("show");
});
$(".category-button").click(function () {
  $('body').toggleClass('toggled');
  $(this).parent().parent().find(".menu").toggleClass("show");
  $('.blur').toggleClass('show');
});
$(".blur").click(function() {
  $(".menu").removeClass("show");
  $(this).removeClass("show");
  $('body').removeClass('toggled');
});

window.onscroll = function showHeader() {
  let header = $(".navbar-fixed");

  if (window.pageYOffset > 100) {
    header.addClass("show__fixed");
  } else {
    header.removeClass("show__fixed");
  }
};
});
