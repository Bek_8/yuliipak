<?php


Route::get('/', 'PageController@basketProducts');
Route::get('/corona/info/{id}', 'SettingController@getCorona');
Route::get('/corona/countries', 'SettingController@getCountries');
Route::get('/corona/useful', 'SettingController@getUseful');
Route::get('/corona/useful/{id}', 'SettingController@getUsefulById');
Route::post('/send/form', 'SettingController@sendForm');
