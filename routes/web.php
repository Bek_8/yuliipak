<?php

Route::group(['middleware' => ['role:admin,manager']], function () {
    Route::get('dashboard', 'DashboardController@index')->name('home');

    //CurrencyController
    Route::get('dashboard/currency', 'CurrencyController@get');
    Route::get('dashboard/currency/update', 'CurrencyController@update');
    Route::get('dashboard/corona', 'SettingController@get');
    Route::get('dashboard/corona/create', 'SettingController@create');
    Route::get('dashboard/corona/edit/{id}', 'SettingController@edit');

    // VendorController
    Route::get('dashboard/vendor', 'VendorController@index');
    Route::get('dashboard/vendor/show/{id}', 'VendorController@show');
    Route::get('dashboard/vendor/create', 'VendorController@create');
    Route::get('dashboard/vendor/edit/{id}', 'VendorController@edit');
    Route::post('dashboard/vendor/store', 'VendorController@store');
    Route::put('dashboard/vendor/update/{id}', 'VendorController@update');
//    Route::delete('dashboard/vendor/delete/{id}', 'VendorController@delete');
    Route::get('dashboard/vendor/removeImage/{id}', 'VendorController@removeImage');
    Route::get('dashboard/vendor/switch/{alias}', 'VendorController@switch');

    Route::get('dashboard/message/order/{id}', 'MessageController@show');

    Route::get('dashboard/orders', 'OrderController@index');
    Route::get('dashboard/orders/show/{id}', 'OrderController@show');
    Route::get('dashboard/order/{id}/change/price', 'OrderController@changePrice');
    Route::get('dashboard/orders/create', 'OrderController@create');
    Route::get('dashboard/orders/edit/{id}', 'OrderController@edit');
    Route::post('dashboard/orders/store', 'OrderController@store');
    Route::put('dashboard/orders/update/{id}', 'OrderController@update');
//    Route::delete('dashboard/orders/delete/{id}', 'OrderController@delete');
    Route::get('dashboard/orders/removeImage/{id}', 'OrderController@removeImage');
    Route::get('dashboard/orders/switch/{id}', 'OrderController@switch');

    Route::get('dashboard/delivery', 'CarrierController@index');
    Route::get('dashboard/delivery/show/{id}', 'CarrierController@show');
    Route::get('dashboard/delivery/create', 'CarrierController@create');
    Route::get('dashboard/delivery/edit/{id}', 'CarrierController@edit');
    Route::post('dashboard/delivery/store', 'CarrierController@store');
    Route::put('dashboard/delivery/update/{id}', 'CarrierController@update');
    Route::get('dashboard/delivery/switch/{id}', 'CarrierController@switch');
    Route::get('dashboard/delivery/removeImage/{id}', 'CarrierController@removeImage');

    Route::get('dashboard/shipping', 'ShippingController@index');
    Route::get('dashboard/shipping/show/{id}', 'ShippingController@show');
    Route::get('dashboard/shipping/create', 'ShippingController@create');
    Route::get('dashboard/shipping/edit/{id}', 'ShippingController@edit');
    Route::post('dashboard/shipping/store', 'ShippingController@store');
    Route::put('dashboard/shipping/update/{id}', 'ShippingController@update');
    Route::get('dashboard/shipping/switch/{id}', 'ShippingController@switch');
    Route::get('dashboard/shipping/removeImage/{id}', 'ShippingController@removeImage');

    Route::get('dashboard/offer/shipping/{id}', 'ShippingOfferController@index');
    Route::get('dashboard/offer/create/shipping', 'ShippingOfferController@create');
    Route::get('dashboard/offer/shipping/edit/{id}', 'ShippingOfferController@edit');
    Route::post('dashboard/offer/shipping/store', 'ShippingOfferController@store');
    Route::put('dashboard/offer/shipping/update/{id}', 'ShippingOfferController@update');

    Route::get('dashboard/vendor/{vendorAlias}/product', 'ProductController@index');
    Route::get('dashboard/vendor/{vendorAlias}/product/show/{productAlias}', 'ProductController@show');
    Route::get('dashboard/vendor/{vendorAlias}/product/switch/{productAlias}', 'ProductController@switch');
    Route::get('dashboard/vendor/{vendorAlias}/product/recommend/switch/{productAlias}', 'ProductController@switchRecommend');
    Route::get('dashboard/vendor/{vendorAlias}/product/create', 'ProductController@create');
    Route::get('dashboard/vendor/{vendorAlias}/product/edit/{productAlias}', 'ProductController@edit');
    Route::post('dashboard/vendor/{vendorAlias}/product/store', 'ProductController@store');
    Route::put('dashboard/vendor/{vendorAlias}/product/update/{productAlias}', 'ProductController@update');
    Route::get('dashboard/product/removeImage/{id}', 'ProductController@removeImage');
    Route::get('dashboard/remove/vendor/{vendorAlias}/product/{productAlias}', 'ProductController@removeProduct');

    Route::get('dashboard/vendor/{vendorAlias}/product/{productAlias}', 'ProductSizeController@index');
    Route::get('dashboard/vendor/{vendorAlias}/product/size/{productAlias}/show/{productSizeAlias}',
        'ProductSizeController@show');
    Route::get('dashboard/vendor/{vendorAlias}/product/size/{productAlias}/switch/{productSizeAlias}',
        'ProductSizeController@switch');
    Route::get('dashboard/vendor/{vendorAlias}/product/size/{productAlias}/create', 'ProductSizeController@create');
    Route::get('dashboard/vendor/{vendorAlias}/product/size/{productAlias}/edit/{productSizeAlias}',
        'ProductSizeController@edit');
    Route::post('dashboard/vendor/{vendorAlias}/product/{productAlias}/store', 'ProductSizeController@store');
    Route::put('dashboard/vendor/{vendorAlias}/product/{productAlias}/update/{productSizeAlias}',
        'ProductSizeController@update');
    Route::get('dashboard/vendor/{vendorAlias}/product/{productAlias}/delete/{productSizeAlias}',
        'ProductSizeController@delete');
    Route::get('dashboard/size/removeImage/{productSizeAlias}', 'ProductSizeController@removeImage');

    Route::get('dashboard/vendor/{vendorAlias}/product/{productAlias}/{productSizeAlias}',
        'ProductSizeTypeController@index');
    Route::get('dashboard/switch/{productSizeTypeId}', 'ProductSizeTypeController@switch');
    Route::get('dashboard/vendor/{vendorAlias}/product/size/{productAlias}/{productSizeAlias}/create',
        'ProductSizeTypeController@create');
    Route::get('dashboard/vendor/{vendorAlias}/product/size/{productAlias}/{productSizeAlias}/edit/{productSizeTypeId}',
        'ProductSizeTypeController@edit');
    Route::post('dashboard/vendor/{vendorAlias}/product/{productAlias}/{productSizeAlias}/store',
        'ProductSizeTypeController@store');
    Route::put('dashboard/vendor/{vendorAlias}/product/{productAlias}/update/{productSizeAlias}/{productSizeTypeId}',
        'ProductSizeTypeController@update');
    Route::get('dashboard/size/type/removeImage/{productSizeAlias}', 'ProductSizeTypeController@removeImage');
    Route::get('dashboard/vendor/{vendorAlias}/product/{productAlias}/delete/{productSizeAlias}/{productSizeTypeId}',
        'ProductSizeTypeController@delete');
});
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/dashboard/banner', 'BannerController@index');
    Route::get('/dashboard/banner/create', 'BannerController@create');
    Route::get('/dashboard/banner/edit/{id}', 'BannerController@edit');
    Route::post('/dashboard/banner/store', 'BannerController@store');
    Route::put('/dashboard/banner/update/{id}', 'BannerController@update');
    Route::delete('/dashboard/banner/delete/{id}', 'BannerController@delete');

//    Route::get('dashboard/corona/delete/{id}', 'SettingController@coronaDelete');
    Route::post('dashboard/corona/store', 'SettingController@store');
    Route::put('dashboard/corona/update/{id}', 'SettingController@update');
    Route::get('dashboard/corona/refresh', 'SettingController@refreshCorona');
    Route::get('dashboard/corona/optimize', 'SettingController@optimize');

    Route::get('/dashboard/country', 'CountryController@index');
    Route::get('/dashboard/country/create', 'CountryController@create');
    Route::get('/dashboard/country/{id}/edit', 'CountryController@edit');
    Route::post('/dashboard/country/store', 'CountryController@store');
    Route::put('/dashboard/country/update/{id}', 'CountryController@update');
    Route::delete('/dashboard/country/delete/{id}', 'CountryController@delete');

    Route::get('/dashboard/country/{id}/city', 'CityController@index');
    Route::get('/dashboard/country/{id}/city/create', 'CityController@create');
    Route::get('/dashboard/country/{id}/city/edit', 'CityController@edit');
    Route::post('/dashboard/country/{id}/city/store', 'CityController@store');
    Route::put('/dashboard/country/{id}/city/update', 'CityController@update');
    Route::delete('/dashboard/country/{id}/city/delete', 'CityController@delete');


    // CategoryController
    Route::get('dashboard/category', 'CategoryController@index');
    Route::get('dashboard/category/show/{id}', 'CategoryController@show');
    Route::get('dashboard/category/create', 'CategoryController@create');
    Route::get('dashboard/category/edit/{id}', 'CategoryController@edit');
    Route::post('dashboard/category/store', 'CategoryController@store');
    Route::get('dashboard/category/toggle/{id}', 'CategoryController@toggleMain');
    Route::put('dashboard/category/update/{id}', 'CategoryController@update');
    Route::get('dashboard/category/removeImage/{id}', 'CategoryController@removeImage');

    Route::get('dashboard/manager', 'ManagerController@index');
    Route::get('dashboard/users', 'ManagerController@users');
    Route::get('dashboard/carriers', 'ManagerController@carriers');
    Route::get('dashboard/manager/create', 'ManagerController@create');
    Route::get('dashboard/user/carrier/create', 'ManagerController@createCarrier');
    Route::get('dashboard/manager/edit/{id}', 'ManagerController@edit');
    Route::post('dashboard/manager/store', 'ManagerController@store');
    Route::put('dashboard/manager/update/{id}', 'ManagerController@update');
    Route::delete('dashboard/manager/delete/{id}', 'ManagerController@delete');


    Route::get('dashboard/collections', 'CollectionController@index');
    Route::get('dashboard/statistics', 'CollectionController@statistic');
    Route::get('dashboard/collections/removeImage/{id}', 'CollectionController@removeImage');
    Route::get('dashboard/collections/create', 'CollectionController@create');
    Route::get('dashboard/collections/edit/{id}', 'CollectionController@edit');
    Route::post('dashboard/collections/store', 'CollectionController@store');
    Route::put('dashboard/collections/update/{id}', 'CollectionController@update');
    Route::delete('dashboard/collections/delete/{id}', 'CollectionController@delete');
});


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::get('/', 'PageController@index');
        Route::get('/collection', 'PageController@collection');
        Route::get('/collection/{id}', 'PageController@showCollection');
        Route::get('/vendor/{alias}', 'PageController@showVendor');
        Route::get('/auth', 'PageController@authorization');
        Route::get('/confirm', 'PageController@confirm');
        Route::get('/set/name', 'PageController@setName');
        Route::get('/top', 'PageController@top');
        Route::get('/basket', 'PageController@basket');
        Route::get('/about', 'PageController@about');
        Route::get('/shipping', 'PageController@shipping');
        Route::get('/rules', 'PageController@rules');
        Route::get('/about/company', 'PageController@company');
        Route::get('/confidential', 'PageController@confidential');
        Route::get('/policy', 'PageController@policy');
        Route::get('/search', 'PageController@search');
        Route::get('/category/{alias?}', 'PageController@category');
        Route::get('/all/category', 'PageController@categoryAll');
        Route::get('/order/{id}', 'PageController@order');
        Route::get('/personal', 'PageController@personal');
        Route::get('/product/{vendorAlias}/{productAlias}', 'PageController@showProduct');
        Route::put('/account/update', 'AuthController@register');
        Route::get('/auth/register', 'PageController@auth');
    });
Route::post('/verification', 'AuthController@verification');
Route::post('/confirm', 'AuthController@auth');
Auth::routes(['register' => false]);


Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'auth']
], function () {
    Route::post('checkout', 'OrderController@checkout');
    Route::get('offer/{id}', 'OrderController@getOffer');
    Route::get('orders', 'PageController@process');
    Route::get('messages', 'PageController@messages');
    Route::get('message/{id}', 'PageController@message');
    Route::post('create/message/product/{id}', 'MessageController@store');
    Route::post('send/message', 'MessageController@send');
    Route::get('order/update/{id}/{type}', 'PageController@updateOrder');
    Route::put('order/carrier/update/{id}', 'PageController@chooseCarrier');
    Route::get('user/logout', 'AuthController@logout');

    Route::put('update/account', 'AuthController@register');
});
