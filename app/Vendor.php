<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Vendor extends Model
{
    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('alias');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function banner()
    {
        return $this->hasMany('App\Banner');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    public function product()
    {
        return $this->hasMany('App\Product');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_vendor')->withTimestamps();
    }

    public function managers()
    {
        return $this->hasMany('App\User','user_id');
    }

    public function getImageAttribute()
    {
        $directory = "uploads/vendor/".$this->id;
        $images = \File::glob($directory."/*.jpg");
        if (count($images) > 0) {
            return $images;
        }
        return [asset('img/logo.svg')];
    }
}
