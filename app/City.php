<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];
    protected $appends = ['name'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    public function getNameAttribute()
    {
        return $this->{'name_'.\App::getLocale()};
    }
}
