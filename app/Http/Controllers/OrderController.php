<?php

namespace App\Http\Controllers;

use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use Auth;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
class OrderController extends Controller
{
    public function index()
    {
        if(Auth::user()->hasRole('admin')) {
            $data = Order::all();
        }
        else if(Auth::user()->hasRole('manager')){
            $data = Order::where('vendor_id',Auth::user()->vendor_id)->get();
        }
        else{
            $data = null;
        }
        return view('backend.order.index',compact('data'));
    }
    public function show($id)
    {
        $data = Order::with('vendor','order_item.product')->findOrFail($id);
        $price = 0;
        foreach ($data->order_item as $item){
            $product = $item->product;
            $price += $product->current * $item->amount ;
        }
        return view('backend.order.show',compact('data','price'));
    }
    public function switch(Request $request, $id)
    {
        $vendor = Order::findOrFail($id);
        $vendor->update([
            'enabled'=> !$vendor->enabled
        ]);
        return redirect()->action('DashboardController@index')->with('success','Изменения успешно внесены');
    }
    public function changePrice($id)
    {
        $order = Order::findOrFail($id);
        if(Auth::user()->vendor_id != $order->vendor_id) abort();
        $order->update([
            'price'=> \request('price')*1
        ]);
        $item_count = $order->order_item()->count();
        $order->order_item()->update([
           'price'=>  request('price') / $item_count
        ]);
        return redirect()->back()->with('success','Изменения успешно внесены');
    }
    public function store(Request $request)
    {
        $request->validate([
            'country_id'=>'required',
            'city_id'=>'required',
            'name'=>'required',
            'address'=> 'required',
            'phone'=> 'required',
            'open_date'=> 'required',
            'lat'=> 'required',
            'long'=> 'required',
            'category_id'=> 'required'
        ]);
        if(!$request['user_id']) $request['user_id'] = Auth::user()->id;
        else $request['user_id'] = User::findOrFail($request['user_id'])->id;   // Check user exist

        $request['open_date'] = Carbon::parse($request['open_date']);
        $vendor = Order::create($request->except('_token','image','category_id'));
        $vendor->categories()->attach($request->category_id);
        return redirect()->action('OrderController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request,$id)
    {
        $request->validate([
            'country_id'=>'required',
            'city_id'=>'required',
            'name'=>'required',
            'description_uz'=> 'required',
            'description_ru'=> 'required',
            'address'=> 'required',
            'phone'=> 'required',
            'open_date'=> 'required',
            'lat'=> 'required',
            'long'=> 'required',
            'category_id'=> 'required'
        ]);
        $vendor = Order::findOrFail($id);
        $vendor->update($request->except('_token','image','user_id','category_id','_method'));
        if($request->category_id){
            $vendor->categories()->sync($request->category_id);
        }
        return redirect()->action('OrderController@index')->with('success','Успешно добавлено');
    }
    public function checkout()
    {
        if(!Auth::check()){
            return redirect()->action('PageController@register');
        }
        \request()->validate([
//            'vendor_id'=> 'required',
//            'user_id'=> 'required',
            'products'=>'required',
//            'address'=>'required',
        ]);
        $products = \request('products');
        // products  [count , prodId , sizeId , typeId , vendorId]
        $get_products = [];
        $countProdSizeTypeVendor = [];
        foreach ($products as $product){
            $explodeString = explode(',',$product);
            $prod = Product::findOrFail($explodeString[1]);
            array_push($get_products,$prod);
            array_push($countProdSizeTypeVendor, $explodeString);
        }
        $vendor = $get_products[0]->vendor;
        $order = Order::create([
            'vendor_id'=> $vendor->id,
            'user_id'=> Auth::user()->id,
            'phone'=> Auth::user()->phone,
            'city_id'=> \request('city_id') *1,
            'price'=> 0,
        ]);
        foreach($get_products as $key => $product){
        if($product->type != 'default'){
            $getSize = $product->size()->where('id',$countProdSizeTypeVendor[$key][2]*1)->firstOrFail();
            $getType = $getSize->type()->where('id',$countProdSizeTypeVendor[$key][3]*1)->firstOrFail();
            $getPrice = json_decode($getType->price,true);
        }
        else{
            $getPrice = json_decode($product->price);
        }
        $prices = [];
        foreach ($getPrice as $price){
            if($countProdSizeTypeVendor[$key][0] >= $price[0]){
                $curr_price = $price[1] * $countProdSizeTypeVendor[$key][0];    //count , price , day
                array_push($prices , $curr_price);
            }
            else{
                $prices = [$price[1] * $countProdSizeTypeVendor[$key][0]];
            }
        }
        $getPrice = $prices[count($prices)-1];
        OrderItem::create([
            'product_id'=> $product->id,
            'order_id'=> $order->id,
            'name'=> $product->name,
            'amount'=> $countProdSizeTypeVendor[$key][0],
            'price'=> $getPrice,
        ]);
        }
        $clearBasket = 0;
        return redirect()->action('PageController@process',compact('clearBasket'));
    }
    public function getOffer($id)
    {
        $order = Order::findOrFail($id);
        $vendor = $order->vendor;
        $pdf = PDF::loadView('frontend.pdf',compact('order','vendor'));
        return $pdf->download('Договор - IpakYili Platform.pdf');
    }
}
