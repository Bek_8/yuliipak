<?php

namespace App\Http\Controllers;

use App\Vendor;
use Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        if($user->hasRole('admin')) {
            $data = Vendor::all();
        }
        else if($user->hasRole('manager')){
            $data = Vendor::where('id',$user->vendor_id)->get();
        }
        else{
            $data = null;
        }
        return view('backend.index',compact('data'));
    }
}
