<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
class CountryController extends Controller
{
    public function index()
    {
        $data = Country::all();
        return view('backend.country.index',compact('data'));
    }
    public function create()
    {
        return view('backend.country.create');
    }
    public function edit($id)
    {
        $data = Country::findOrFail($id);
        return view('backend.country.edit',compact('data','id'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
        ]);
        Country::create([
            'name_ru' => $request->name_ru,
            'name_uz' => $request->name_uz,
            'flag' => $request->flag
        ]);
        return redirect()->action('CountryController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
        ]);
        Country::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('CountryController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Country::findOrFail($id);
        $hotel->delete();
        return redirect()->action('CountryController@index')->with('success','Успешно удален');
    }
}
