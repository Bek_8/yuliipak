<?php

namespace App\Http\Controllers;

use App\User;
use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Collection;
use App\Product;
use Intervention\Image\ImageManagerStatic as Image;

class CollectionController extends Controller
{

    private $validation = [
        'name_ru' => 'required',
        'name_uz' => 'required',
        'description_ru' => 'required',
        'description_uz' => 'required',
        'product_id' => 'required',
    ];

    public function statistic()
    {
        $products = Product::all();
        $vendors = Vendor::all();
        $users = User::all();
        return view('backend.collection.statistic', compact('products', 'vendors', 'users'));
    }

    public function index()
    {
        $data = Collection::paginate(20);
        return view('backend.collection.index', compact('data'));
    }

    public function create()
    {
        $products = Product::all();
        return view('backend.collection.create', compact('products'));
    }

    public function edit($id)
    {
        $data = Collection::findOrFail($id);
        $products = Product::all();
        $directory = "uploads/collection/" . $id;
        $images = \File::glob($directory . "/*.jpg");
        return view('backend.collection.edit', compact('data', 'products', 'images'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);

        $collection = Collection::create($request->except('_token', 'image', 'product_id', 'jfiler-items-exclude-image-0'));
        $collection->products()->attach($request->product_id);
        if ($request->file('image')) {
            $this->createFolder('collection', $collection->id);
            $images = $request->file('image');
            $this->storeImages($images, $collection->id);
        }
        return redirect()->action('CollectionController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        $collection = Collection::where('id', $id)->firstOrFail();
        $collection->update($request->except('_token', 'image', 'product_id', '_method',
            'jfiler-items-exclude-image-0'));
        if ($request->product_id) {
            $collection->products()->sync($request->product_id);
        }
        if ($request->file('image')) {
            $this->createFolder('collection', $collection->id);
            $images = $request->file('image');
            $this->storeImages($images, $collection->id);
        }

        return redirect()->action('CollectionController@index')->with('success', 'Успешно изменено');
    }

    public function delete($id)
    {
        $collection = Collection::findOrFail($id);
        $collection->products()->detach();
        $collection->delete();
        return redirect()->action('CollectionController@index')->with('success', 'Успешно');
    }

    public function createFolder($directory, $id)
    {
        $path = public_path('uploads/' . $directory . '/' . $id);
        $directory_path = public_path('uploads/' . $directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $id)
    {
        foreach ($images as $key => $image) {
            $filename = $key . '.jpg';
            $path = public_path('uploads/collection/' . $id . '/' . $filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
                ->fit(370, 300)
                ->save($path);
        }
        return 1;
    }

    public function removeImage($id)
    {
        $file_name = $id . '.jpg';
        $pathToDestroy = public_path('uploads/collection/' . $id . '/' . $file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }
}
