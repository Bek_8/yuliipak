<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
}
