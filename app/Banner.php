<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $guarded = [];
    protected $appends = ['image'];

    public function vendor()
    {
        $this->belongsTo('App\Vendor');
    }

    public function getImageAttribute()
    {
        $directory = "uploads/banner/" . $this->id;
        $images = \File::glob($directory . "/*.jpg");
        if (count($images) > 0) {
            return $images;
        }
        return [asset('img/default.jpg')];
    }
}
