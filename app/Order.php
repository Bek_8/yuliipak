<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }
    public function product()
    {
        return $this->hasMany('App\Product');
    }
    public function order_item()
    {
        return $this->hasMany('App\OrderItem');
    }
    public function messages()
    {
        return $this->hasMany('App\Message');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function getShipping()
    {
        $from = $this->vendor->city_id;
        $to = $this->city_id;
        return Shipping::where([['from_id',$from],['to_id',$to]])->with('offer')->get();
    }
}
