<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Category extends Model
{
    use HasSlug;

    protected $guarded = [];
    protected $appends = ['name', 'image'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name_ru')
            ->saveSlugsTo('alias');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function sub_category()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function parent_category()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vendors()
    {
        return $this->belongsToMany('App\Vendor', 'categories_vendor');
    }

    public function products()
    {
        return $this->hasMany('App\Product')->where('enabled',1);
    }

    public function main_products()
    {
        $sub_id = $this->sub_category()->pluck('id')->toArray();
        array_push($sub_id, $this->id);
        $categories = array_unique($sub_id);
        return Product::whereIn('category_id', $categories)->where('enabled', 1)->whereHas('vendor', function ($query) {
            $query->where('enabled', 1);
        })->take(10)->get();
    }

    public function getNameAttribute()
    {
        return $this->{'name_'.\App::getLocale()};
    }

    public function getImageAttribute()
    {
        $directory = "uploads/categories/".$this->id;
        $images = \File::glob($directory."/*.jpg");
        if (count($images) > 0) {
            return $images[0];
        }
        return 'img/category.png';
    }
}
